import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FirebaseModule } from '../firebase/firebaseModule';
import { AccountDeviceToken } from './entities';
import { AccountDeviceTokenResolver } from './resolvers/account-device-token.resolver';
import { AccountDeviceTokenService, NotifyService } from './services';

@Module({
  imports: [TypeOrmModule.forFeature([AccountDeviceToken]), FirebaseModule],
  providers: [AccountDeviceTokenService, NotifyService, AccountDeviceTokenResolver],
  controllers: [],
  exports: [NotifyService],
})
export class NotificationModule {}
