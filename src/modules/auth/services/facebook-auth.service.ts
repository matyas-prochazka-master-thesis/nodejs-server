import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { Repository } from 'typeorm';

import { Account } from '../../account/entities';
import { AccountSourceEnum } from '../../account/enums';
import { IAccount } from '../../account/interfaces';
import { FacebookAuthDto } from '../dto/facebook-auth.dto';
import { AuthService } from './auth.service';

@Injectable()
export class FacebookAuthService {
  constructor(
    @InjectRepository(Account)
    private accountRepository: Repository<Account>,
    private authService: AuthService,
  ) {}

  async auth({ facebookAuthDto }: { facebookAuthDto: FacebookAuthDto }) {
    const { accessToken, source } = facebookAuthDto;

    const { data } = await axios({
      url: 'https://graph.facebook.com/me',
      method: 'get',
      params: {
        fields: ['id', 'email'].join(','),
        access_token: accessToken,
      },
    });

    return this.authService.createToken(
      await this.getOrCreate({ facebookId: data.id, email: data.email.toLowerCase(), source }),
    );
  }

  private async getOrCreate({
    facebookId,
    email,
    source,
  }: {
    facebookId: string;
    email: string;
    source: AccountSourceEnum | null;
  }): Promise<IAccount> {
    let account = await this.accountRepository.findOne({ facebookId });

    if (account) {
      return account;
    }

    account = await this.accountRepository.findOne({ email });
    if (!account) {
      account = new Account({ email, source });
    }

    account.facebookId = facebookId;

    return this.accountRepository.save(account);
  }
}
