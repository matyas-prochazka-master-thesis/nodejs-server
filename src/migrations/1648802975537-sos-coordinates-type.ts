import { MigrationInterface, QueryRunner } from 'typeorm';

export class sosCoordinatesType1648802975537 implements MigrationInterface {
  name = 'sosCoordinatesType1648802975537';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sos" DROP COLUMN "initialLatitude"`);
    await queryRunner.query(`ALTER TABLE "sos" ADD "initialLatitude" numeric`);
    await queryRunner.query(`ALTER TABLE "sos" DROP COLUMN "initialLongitude"`);
    await queryRunner.query(`ALTER TABLE "sos" ADD "initialLongitude" numeric`);
    await queryRunner.query(`ALTER TABLE "sos_position" DROP COLUMN "latitude"`);
    await queryRunner.query(`ALTER TABLE "sos_position" ADD "latitude" numeric NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sos_position" DROP COLUMN "longitude"`);
    await queryRunner.query(`ALTER TABLE "sos_position" ADD "longitude" numeric NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sos_position" DROP COLUMN "longitude"`);
    await queryRunner.query(`ALTER TABLE "sos_position" ADD "longitude" integer NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sos_position" DROP COLUMN "latitude"`);
    await queryRunner.query(`ALTER TABLE "sos_position" ADD "latitude" integer NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sos" DROP COLUMN "initialLongitude"`);
    await queryRunner.query(`ALTER TABLE "sos" ADD "initialLongitude" integer`);
    await queryRunner.query(`ALTER TABLE "sos" DROP COLUMN "initialLatitude"`);
    await queryRunner.query(`ALTER TABLE "sos" ADD "initialLatitude" integer`);
  }
}
