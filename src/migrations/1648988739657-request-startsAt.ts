import { MigrationInterface, QueryRunner } from 'typeorm';

export class requestStartsAt1648988739657 implements MigrationInterface {
  name = 'requestStartsAt1648988739657';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "checkin_request" ADD "startsAt" TIMESTAMP WITH TIME ZONE`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "checkin_request" DROP COLUMN "startsAt"`);
  }
}
