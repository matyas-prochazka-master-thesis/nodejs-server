import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class SosPositionUpdateDto {
  @Field()
  @IsNotEmpty()
  readonly idFromClient: string;

  @Field()
  @IsNotEmpty()
  readonly latitude: number;

  @Field()
  @IsNotEmpty()
  readonly longitude: number;
}
