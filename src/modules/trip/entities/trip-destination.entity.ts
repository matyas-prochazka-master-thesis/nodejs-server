import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { ITripDestination } from '../interfaces';

@ObjectType()
@Entity()
export class TripDestination implements ITripDestination {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Account)
  @ManyToOne(() => Account)
  account: IAccount;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column('decimal')
  latitude: number;

  @Field()
  @Column('decimal')
  longitude: number;

  @Field()
  @Column({ default: false })
  favourite: boolean;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  constructor(props: { account: IAccount; name: string; latitude: number; longitude: number }) {
    this.account = props?.account;
    this.name = props?.name;
    this.latitude = props?.latitude;
    this.longitude = props?.longitude;
  }
}
