import {MigrationInterface, QueryRunner} from "typeorm";

export class guardianAddedAt1648141647595 implements MigrationInterface {
    name = 'guardianAddedAt1648141647595'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "guardian" ADD "addedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "guardian" DROP COLUMN "addedAt"`);
    }

}
