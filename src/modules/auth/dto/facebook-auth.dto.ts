import { Field, InputType } from '@nestjs/graphql';

import { AccountSourceEnum } from '../../account/enums';

@InputType()
export class FacebookAuthDto {
  @Field()
  readonly accessToken: string;

  @Field(() => AccountSourceEnum, { nullable: true })
  readonly source: AccountSourceEnum | null;
}
