import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class GuardianAddDto {
  @Field()
  @IsNotEmpty()
  readonly username: string;
}
