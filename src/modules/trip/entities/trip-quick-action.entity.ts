import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { TripQuickActionDto } from '../dto';
import { TripStateEnum } from '../enums';
import { ITrip, ITripActivity, ITripDestination, ITripQuickAction } from '../interfaces';
import { TripActivity } from './trip-activity.entity';
import { TripDestination } from './trip-destination.entity';

@ObjectType()
export class TripQuickAction implements ITripQuickAction {
  @Field()
  id: string;

  @Field()
  name: string;

  @Field()
  icon: string;

  constructor(props: { id: string; name: string; icon: string }) {
    this.id = props?.id;
    this.name = props?.name;
    this.icon = props?.icon;
  }
}
