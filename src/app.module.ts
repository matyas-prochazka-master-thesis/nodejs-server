import { Module } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GraphQLModule } from '@nestjs/graphql';
import { ScheduleModule } from '@nestjs/schedule';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';

import { configService } from './config/config.service';
import { AccountModule } from './modules/account/account.module';
import { AuthModule } from './modules/auth/auth.module';
import { CheckinModule } from './modules/checkin/checkin.module';
import { EmitterModule } from './modules/emitter/emitter.module';
import { FirebaseModule } from './modules/firebase/firebaseModule';
import { GuardianModule } from './modules/guardian/guardian.module';
import { MailModule } from './modules/mail/mail.module';
import { NotificationModule } from './modules/notification/notification.module';
import { SosModule } from './modules/sos/sos.module';
import { StorageModule } from './modules/storage/storage.module';
import { TripModule } from './modules/trip/trip.module';
import { VoterModule } from './modules/voter/voter.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    GraphQLModule.forRoot({
      installSubscriptionHandlers: true,
      context: ({ req, connection }) => (connection ? { req: { headers: connection.context } } : { req }),
      debug: true,
      playground: true,
      introspection: configService.getGqlIntrospection(),
      autoSchemaFile: 'schema.gql',
      subscriptions: {
        'subscriptions-transport-ws': {
          keepAlive: 15000,
        },
      },
      formatError: (error) => {
        console.log(error);

        return error;
      },
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    EventEmitterModule.forRoot(),
    ScheduleModule.forRoot(),
    AccountModule,
    AuthModule,
    CheckinModule,
    EmitterModule,
    FirebaseModule,
    GuardianModule,
    MailModule,
    NotificationModule,
    SosModule,
    StorageModule,
    TripModule,
    VoterModule,
  ],
})
export class AppModule {}
