import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Cron, CronExpression } from '@nestjs/schedule';
import axios from 'axios';

@Injectable()
export class EmitterService {
  constructor(private readonly eventEmitter: EventEmitter2) {}

  @Cron(CronExpression.EVERY_10_MINUTES)
  async keepAlive(): Promise<void> {
    axios
      .get('https://mattproch-safie.herokuapp.com/gdpr.pdf')
      .then(() => {})
      .catch(() => {});
  }
}
