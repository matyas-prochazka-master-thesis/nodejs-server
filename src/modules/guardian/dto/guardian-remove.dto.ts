import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class GuardianRemoveDto {
  @Field(() => ID!)
  @IsNotEmpty()
  readonly guardianId: number;
}
