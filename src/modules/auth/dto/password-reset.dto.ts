import { Field, InputType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

@InputType()
export class PasswordResetDto {
  @ApiProperty()
  @IsEmail()
  @Field()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @Field()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  @Field()
  password: string;
}
