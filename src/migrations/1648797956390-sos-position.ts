import { MigrationInterface, QueryRunner } from 'typeorm';

export class sosPosition1648797956390 implements MigrationInterface {
  name = 'sosPosition1648797956390';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "sos_position" ("id" SERIAL NOT NULL, "latitude" integer NOT NULL, "longitude" integer NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "sosId" integer, CONSTRAINT "PK_08ac6db7729d2088892de641277" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "sos" ADD "initialLatitude" integer NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sos" ADD "initialLongitude" integer NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "sos_position" ADD CONSTRAINT "FK_c6bfba85595cda9a9b46376e40b" FOREIGN KEY ("sosId") REFERENCES "sos"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sos_position" DROP CONSTRAINT "FK_c6bfba85595cda9a9b46376e40b"`);
    await queryRunner.query(`ALTER TABLE "sos" DROP COLUMN "initialLongitude"`);
    await queryRunner.query(`ALTER TABLE "sos" DROP COLUMN "initialLatitude"`);
    await queryRunner.query(`DROP TABLE "sos_position"`);
  }
}
