import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class TripDestinationCreateDto {
  @Field()
  @IsNotEmpty()
  readonly name: string;

  @Field()
  @IsNotEmpty()
  readonly latitude: number;

  @Field()
  @IsNotEmpty()
  readonly longitude: number;
}
