import { registerEnumType } from '@nestjs/graphql';

export enum TripActivityKeyEnum {
  MESSAGE = 'message',
  PHOTO = 'photo',
  QUICK_ACTION = 'quick_action',
  POSITION = 'position',
  CHECK = 'check',
}

registerEnumType(TripActivityKeyEnum, { name: 'TripActivityKey' });
