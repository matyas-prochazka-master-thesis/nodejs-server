import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as fs from 'fs';
import * as mime from 'mime-types';
import * as pkgcloud from 'pkgcloud';
import sharp from 'sharp';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

import { configService } from '../../../config/config.service';
import { BadRequestException, InternalErrorException, NotFoundException } from '../../../shared/exceptions';
import { IAccount } from '../../account/interfaces';
import { File } from '../entities';
import { IFile } from '../interfaces';

@Injectable()
export class StorageService {
  static imageExtensions = ['gif', 'jpg', 'jpeg', 'png'];

  private readonly storage;

  private readonly config;

  private readonly client;

  private readonly prefix;

  constructor(
    @InjectRepository(File)
    private readonly fileRepository: Repository<File>,
  ) {
    this.storage = configService.getStorage();

    if (this.storage === 's3') {
      this.config = configService.getS3();

      this.client = pkgcloud.storage.createClient({
        provider: 'amazon',
        keyId: this.config.accessKeyId,
        key: this.config.secretAccessKey,
        region: this.config.region,
        version: this.config.version,
      });

      this.prefix = this.config.prefix;
    }
  }

  async find(fileId: number) {
    const file = await this.fileRepository.findOne(fileId);

    if (!file) {
      throw new NotFoundException({ message: 'file not found' });
    }

    return file;
  }

  async uploadBase64(account: IAccount, base64: string, folder: string): Promise<IFile> {
    const matches = base64.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

    if (!(matches instanceof Array)) {
      throw new BadRequestException({ message: 'Invalid base64 string [NOT ARRAY]' });
    }

    if (matches.length !== 3) {
      throw new BadRequestException({ message: 'Invalid base64 string [ARRAY LENGTH]' });
    }

    let base64Buffer;

    try {
      base64Buffer = Buffer.from(matches[2], 'base64');
    } catch (e) {
      throw new BadRequestException({ message: 'Invalid base64 string [BUFFER]' });
    }

    let mimeType = matches[1];
    const extension = mime.extension(mimeType);

    if (!StorageService.imageExtensions.includes(extension)) {
      throw new BadRequestException({ message: 'bad image format' });
    }

    const name = `${Date.now()}-${uuidv4()}.jpg`;
    const tmpPath = `.tmp/${name}`;
    const path = folder + name;

    if (!fs.existsSync('.tmp/')) {
      fs.mkdirSync('.tmp/');
    }

    // fs.writeFileSync(tmpPath, base64, 'base64');

    try {
      await sharp(base64Buffer)
        .resize(800, null, {
          withoutEnlargement: true,
        })
        .jpeg({ quality: 80 })
        .toFile(tmpPath);
    } catch (e) {
      console.log(e);

      throw new BadRequestException({ message: 'Invalid base64 string [SHARP]' });
    }

    mimeType = 'image/jpeg';

    try {
      if (this.storage === 's3') {
        await this.uploadS3(tmpPath, path);
      }
    } catch (e) {
      fs.unlinkSync(tmpPath);
      throw e;
    }

    fs.unlinkSync(tmpPath);

    const file = new File(path, mimeType);
    await this.fileRepository.insert(file);

    return file;
  }

  private async uploadS3(tmpPath: string, path: string): Promise<void> {
    const readStream = fs.createReadStream(tmpPath);

    const writeStream = this.client.upload({
      container: this.config.bucket,
      remote: this.prefix + path,
    });

    await new Promise((resolve, reject) => {
      readStream.on('error', () => {
        // writeStream.close();
        reject(new InternalErrorException({ message: 'file read error' }));
      });

      writeStream.on('error', (err) => {
        console.log(err);
        // readStream.close();
        reject(new InternalErrorException({ message: 'file write error' }));
      });

      writeStream.on('success', () => {
        resolve(null);
      });

      readStream.pipe(writeStream);
    });
  }

  async removeById(fileId: number) {
    const file = await this.find(fileId);

    return this.remove(file);
  }

  async remove(file: IFile): Promise<void> {
    if (this.storage === 's3') {
      return this.removeS3(file);
    }

    return this.removeLocal(file);
  }

  async removeS3(file: IFile): Promise<void> {
    await this.fileRepository.remove(file);

    return new Promise((resolve, reject) => {
      this.client.removeFile(this.config.bucket, this.prefix + file.path, (error) => {
        if (error) return reject(new InternalErrorException({ message: 'file remove error' }));

        return resolve();
      });
    });
  }

  async removeLocal(file: IFile): Promise<void> {
    await this.fileRepository.remove(file);
  }
}
