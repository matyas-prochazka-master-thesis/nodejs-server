import { registerEnumType } from '@nestjs/graphql';

export enum CheckinStateEnum {
  REQUESTED = 'requested',
  CHECKED = 'checked',
}

registerEnumType(CheckinStateEnum, { name: 'CheckinState' });
