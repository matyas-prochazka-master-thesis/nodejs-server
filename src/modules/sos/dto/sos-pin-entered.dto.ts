import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class SosPinEnteredDto {
  @Field()
  @IsNotEmpty()
  readonly idFromClient: string;
}
