import { registerEnumType } from '@nestjs/graphql';

export enum TripStateEnum {
  STARTED = 'started',
  FINISHED = 'finished',
}

registerEnumType(TripStateEnum, { name: 'TripState' });
