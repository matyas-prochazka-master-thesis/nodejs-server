import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class SosButtonPressedDto {
  @Field()
  @IsNotEmpty()
  readonly idFromClient: string;

  @Field({ nullable: true })
  readonly latitude: number | null;

  @Field({ nullable: true })
  readonly longitude: number | null;
}
