import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { GuardianService } from '../../guardian/services';
import { FolderPrefixEnum } from '../../storage/enums';
import { StorageService } from '../../storage/services';
import { TripAttributes } from '../../voter/attributes';
import { VoterService } from '../../voter/voter.service';
import { ActivityCreateDto, ActivityMessagePayload, ActivityPhotoPayload, ActivityQuickActionPayload } from '../dto';
import { Trip, TripActivity } from '../entities';
import { TripActivityKeyEnum } from '../enums/trip-activity-key.enum';
import tripExceptions from '../exceptions/trip-exceptions';
import { ITripActivity } from '../interfaces';
import tripNotifications from '../notifications/trip-notifications';
import { TripService } from './trip.service';

@Injectable()
export class TripActivityService {
  constructor(
    @InjectRepository(Trip)
    private readonly tripRepository: Repository<Trip>,
    @InjectRepository(TripActivity)
    private readonly tripActivityRepository: Repository<TripActivity>,
    private readonly tripService: TripService,
    private readonly guardianService: GuardianService,
    private readonly voterService: VoterService,
    private readonly storageService: StorageService,
  ) {}

  async getActivitiesByTrip({ account, tripId }: { account: IAccount; tripId: number }): Promise<ITripActivity[]> {
    const trip = await this.tripService.findTrip({ tripId });

    if (!(await this.voterService.isGranted(TripAttributes.VIEW, trip, account))) {
      throw tripExceptions.createCannotViewTrip();
    }

    return this.tripActivityRepository.find({
      relations: ['attachment'],
      where: {
        trip,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async addMessageToTrip({
    account,
    tripId,
    activityCreateDto,
    activityMessagePayload,
  }: {
    account: IAccount;
    tripId: number;
    activityCreateDto: ActivityCreateDto;
    activityMessagePayload: ActivityMessagePayload;
  }): Promise<ITripActivity> {
    const trip = await this.tripService.findTrip({ tripId });

    if (!(await this.voterService.isGranted(TripAttributes.UPDATE, trip, account))) {
      throw tripExceptions.createCannotUpdateTrip();
    }

    const { latitude, longitude, batteryLevel } = activityCreateDto;

    const activity = await this.tripActivityRepository.save(
      new TripActivity({
        trip,
        key: TripActivityKeyEnum.MESSAGE,
        latitude,
        longitude,
        batteryLevel,
        payload: JSON.stringify(activityMessagePayload),
        attachment: null,
      }),
    );

    const notification = tripNotifications.createAddedMessageToTrip({ trip, message: activityMessagePayload.message });

    this.guardianService.notifyGuardians({ account, notification });

    return activity;
  }

  async addPhotoToTrip({
    account,
    tripId,
    activityCreateDto,
    activityPhotoPayload,
  }: {
    account: IAccount;
    tripId: number;
    activityCreateDto: ActivityCreateDto;
    activityPhotoPayload: ActivityPhotoPayload;
  }): Promise<ITripActivity> {
    const trip = await this.tripService.findTrip({ tripId });

    if (!(await this.voterService.isGranted(TripAttributes.UPDATE, trip, account))) {
      throw tripExceptions.createCannotUpdateTrip();
    }

    const { latitude, longitude, batteryLevel } = activityCreateDto;
    const { photo } = activityPhotoPayload;

    const attachment = await this.storageService.uploadBase64(account, photo, FolderPrefixEnum.TRIP);

    const activity = await this.tripActivityRepository.save(
      new TripActivity({
        trip,
        key: TripActivityKeyEnum.PHOTO,
        latitude,
        longitude,
        batteryLevel,
        payload: JSON.stringify({}),
        attachment,
      }),
    );

    const notification = tripNotifications.createAddedPhotoToTrip({ trip });

    this.guardianService.notifyGuardians({ account, notification });

    return activity;
  }

  async addQuickActionToTrip({
    account,
    tripId,
    activityCreateDto,
    activityQuickActionPayload,
  }: {
    account: IAccount;
    tripId: number;
    activityCreateDto: ActivityCreateDto;
    activityQuickActionPayload: ActivityQuickActionPayload;
  }): Promise<ITripActivity> {
    const trip = await this.tripService.findTrip({ tripId });

    if (!(await this.voterService.isGranted(TripAttributes.UPDATE, trip, account))) {
      throw tripExceptions.createCannotUpdateTrip();
    }

    const { latitude, longitude, batteryLevel } = activityCreateDto;
    const { name, icon, currentQuickActionIndex } = activityQuickActionPayload;

    const activity = await this.tripActivityRepository.save(
      new TripActivity({
        trip,
        key: TripActivityKeyEnum.QUICK_ACTION,
        latitude,
        longitude,
        batteryLevel,
        payload: JSON.stringify({ name, icon }),
        attachment: null,
      }),
    );

    if (currentQuickActionIndex) {
      trip.currentQuickActionIndex = currentQuickActionIndex;

      await this.tripRepository.save(trip);
    }

    const notification = tripNotifications.createAddedQuickActionToTrip({ trip, quickActionName: name });

    this.guardianService.notifyGuardians({ account, notification });

    return activity;
  }
}
