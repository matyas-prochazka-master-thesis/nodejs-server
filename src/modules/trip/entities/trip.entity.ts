import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { TripQuickActionDto } from '../dto';
import { TripStateEnum } from '../enums';
import { ITrip, ITripActivity, ITripDestination } from '../interfaces';
import { TripActivity } from './trip-activity.entity';
import { TripDestination } from './trip-destination.entity';
import { TripQuickAction } from './trip-quick-action.entity';

@ObjectType()
@Entity()
export class Trip implements ITrip {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Account)
  @ManyToOne(() => Account)
  account: IAccount;

  @Field(() => TripStateEnum)
  @Column()
  state: TripStateEnum;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  shouldArriveAt: Date | null;

  @Field(() => TripDestination, { nullable: true })
  @ManyToOne(() => TripDestination, { nullable: true })
  destination: ITripDestination | null;

  @Field(() => [TripQuickAction])
  @Column('json')
  quickActions: TripQuickAction[];

  @Field(() => Int)
  @Column({ default: 0 })
  currentQuickActionIndex: number;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @Field(() => [TripActivity])
  @OneToMany(() => TripActivity, (activity) => activity.trip)
  activities: ITripActivity[];

  constructor(props: {
    account: IAccount;
    shouldArriveAt: Date | null;
    destination: ITripDestination | null;
    quickActions: TripQuickAction[];
  }) {
    this.account = props?.account;
    this.shouldArriveAt = props?.shouldArriveAt;
    this.destination = props?.destination;
    this.quickActions = props?.quickActions;

    this.state = TripStateEnum.STARTED;
  }
}
