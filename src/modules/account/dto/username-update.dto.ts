import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class UsernameUpdateDto {
  @Field()
  @IsNotEmpty()
  readonly username: string;
}
