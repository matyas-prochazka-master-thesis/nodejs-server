import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { GuardianService } from '../../guardian/services';
import { SosAttributes } from '../../voter/attributes';
import { VoterService } from '../../voter/voter.service';
import { SosButtonPressedDto, SosButtonReleasedDto, SosPinEnteredDto, SosPositionUpdateDto } from '../dto';
import { SosPosition, Sos } from '../entities';
import { SosStateEnum } from '../enums';
import sosExceptions from '../exceptions/sos-exceptions';
import { ISos } from '../interfaces';

@Injectable()
export class SosService {
  constructor(
    @InjectRepository(Sos)
    private readonly sosRepository: Repository<Sos>,
    @InjectRepository(SosPosition)
    private readonly sosPositionRepository: Repository<SosPosition>,
    private readonly guardianService: GuardianService,
    private readonly voterService: VoterService,
  ) {}

  // idFromClient is used to reference the entity so that mobile app does not have to wait until the first request is satisfied
  async findByIdFromClient({ idFromClient }: { idFromClient: string }): Promise<ISos> {
    const sos = await this.sosRepository.findOne({
      relations: ['account'],
      where: {
        idFromClient,
      },
    });

    if (!sos) {
      throw sosExceptions.createSosNotFound();
    }

    return sos;
  }

  async sosButtonPressed({
    account,
    sosButtonPressedDto,
  }: {
    account: IAccount;
    sosButtonPressedDto: SosButtonPressedDto;
  }): Promise<void> {
    const { idFromClient, latitude, longitude } = sosButtonPressedDto;

    const sos = new Sos({
      account,
      idFromClient,
      initialLatitude: latitude,
      initialLongitude: longitude,
    });

    sos.state = SosStateEnum.HOLDING;
    sos.buttonPressedAt = new Date();

    await this.sosRepository.save(sos);
  }

  async sosButtonReleased({
    account,
    sosButtonReleasedDto,
  }: {
    account: IAccount;
    sosButtonReleasedDto: SosButtonReleasedDto;
  }): Promise<void> {
    const { idFromClient } = sosButtonReleasedDto;

    const sos = await this.findByIdFromClient({ idFromClient });

    if (!(await this.voterService.isGranted(SosAttributes.UPDATE, sos, account))) {
      throw sosExceptions.createCannotUpdateSos();
    }

    sos.state = SosStateEnum.RELEASED;
    sos.buttonReleasedAt = new Date();

    await this.sosRepository.save(sos);
  }

  async sosPinEntered({
    account,
    sosPinEnteredDto,
  }: {
    account: IAccount;
    sosPinEnteredDto: SosPinEnteredDto;
  }): Promise<void> {
    const { idFromClient } = sosPinEnteredDto;

    const sos = await this.findByIdFromClient({ idFromClient });

    if (!(await this.voterService.isGranted(SosAttributes.UPDATE, sos, account))) {
      throw sosExceptions.createCannotUpdateSos();
    }

    if (sos.state === SosStateEnum.EMERGENCY) {
      return;
    }

    sos.state = SosStateEnum.PIN_ENTERED;
    sos.pinEnteredAt = new Date();

    await this.sosRepository.save(sos);
  }

  async sosPositionUpdate({
    account,
    sosPositionUpdateDto,
  }: {
    account: IAccount;
    sosPositionUpdateDto: SosPositionUpdateDto;
  }): Promise<void> {
    const { idFromClient, latitude, longitude } = sosPositionUpdateDto;

    const sos = await this.findByIdFromClient({ idFromClient });

    if (!(await this.voterService.isGranted(SosAttributes.UPDATE, sos, account))) {
      throw sosExceptions.createCannotUpdateSos();
    }

    const sosPosition = new SosPosition({ sos, latitude, longitude });

    await this.sosPositionRepository.save(sosPosition);
  }

  async getSosHistory({ account }: { account: IAccount }): Promise<Sos[]> {
    return this.sosRepository.find({
      where: {
        account,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async getLatestByAccount({ account, targetAccountId }: { account: IAccount; targetAccountId: number }): Promise<Sos> {
    const sos = await this.sosRepository.findOne({
      relations: ['account', 'account.profilePicture'],
      where: {
        account: {
          id: targetAccountId,
        },
      },
      order: {
        createdAt: 'DESC',
      },
    });

    if (!sos || !(await this.voterService.isGranted(SosAttributes.VIEW, sos, account))) {
      throw sosExceptions.createCannotViewSos();
    }

    return sos;
  }

  async getSosPositions({ account, sosId }: { account: IAccount; sosId: number }): Promise<SosPosition[]> {
    const sos = await this.sosRepository.findOne(sosId, {
      relations: ['account'],
    });

    if (!sos || !(await this.voterService.isGranted(SosAttributes.VIEW, sos, account))) {
      throw sosExceptions.createCannotViewSos();
    }

    return this.sosPositionRepository.find({
      where: {
        sos,
      },
    });
  }

  async getSosesOfAccountsIAmGuardianOf({ account }: { account: IAccount }): Promise<Sos[]> {
    const guardians = await this.guardianService.getGuardiansByAccount({ account });

    return this.sosRepository.find({
      relations: ['account', 'account.profilePicture'],
      where: {
        account: In(guardians.map((guardian) => guardian.guardian.id)), // Need to map to ID not the whole entity
      },
      order: {
        createdAt: 'DESC',
      },
      take: 30,
    });
  }
}
