import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

// REST get refresh token from authentication header
@Injectable()
export class AuthJwtRefreshGuard extends AuthGuard('jwt-refresh') {}
