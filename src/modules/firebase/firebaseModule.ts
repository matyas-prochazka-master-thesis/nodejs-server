import { Module } from '@nestjs/common';

import { FirebaseService } from './services';

@Module({
  imports: [],
  providers: [FirebaseService],
  controllers: [],
  exports: [FirebaseService],
})
export class FirebaseModule {}
