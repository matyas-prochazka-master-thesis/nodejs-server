import { BadRequestException, NotFoundException } from '../../../shared/exceptions';

const createAccountNotFound = () => {
  return new NotFoundException({ message: 'account not found' });
};

const createEmailMustBeUnique = () => {
  return new BadRequestException({ message: 'Email must be unique.' });
};

const createUsernameMustBeUnique = () => {
  return new BadRequestException({ message: 'Username must be unique.' });
};

export default {
  createAccountNotFound,
  createEmailMustBeUnique,
  createUsernameMustBeUnique,
};
