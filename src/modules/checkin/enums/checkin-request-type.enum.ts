import { registerEnumType } from '@nestjs/graphql';

export enum CheckinRequestTypeEnum {
  ONE_TIME = 'one_time',
  PERIODICAL = 'periodical',
}

registerEnumType(CheckinRequestTypeEnum, { name: 'CheckinRequestType' });
