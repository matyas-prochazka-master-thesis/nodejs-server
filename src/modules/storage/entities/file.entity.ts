import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

import { IFile } from '../interfaces';

@ObjectType()
@Entity()
export class File implements IFile {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  path: string;

  @Field()
  @Column()
  mimeType: string;

  @Field()
  @Column()
  size: number;

  @Field()
  @Column()
  addedAt: Date;

  constructor(path: string, mimeType: string) {
    this.path = path;
    this.mimeType = mimeType;
    this.size = 0;

    this.addedAt = new Date();
  }
}
