import { Inject } from '@nestjs/common';

import { IAccount } from '../../account/interfaces';
import { GuardianService } from '../../guardian/services';
import { TripAttributes } from '../../voter/attributes';
import { IVoter } from '../../voter/interfaces';
import { ITrip, ITripDestination } from '../interfaces';

export class TripVoter extends IVoter {
  @Inject(GuardianService)
  private readonly guardianService: GuardianService;

  async supports(attribute: string): Promise<boolean> {
    return (<any>Object).values(TripAttributes).includes(attribute);
  }

  async voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    switch (attribute) {
      case TripAttributes.UPDATE:
        return this.canUpdate(account, subject);
      case TripAttributes.VIEW:
        return this.canView(account, subject);
      case TripAttributes.DESTINATION_VIEW:
        return this.canViewDestination(account, subject);
      case TripAttributes.DESTINATION_FAVOURITE:
        return this.canFavouriteDestination(account, subject);
    }

    return false;
  }

  async canUpdate(account: IAccount, trip: ITrip): Promise<boolean> {
    return account.id === trip.account.id;
  }

  async canView(account: IAccount, trip: ITrip): Promise<boolean> {
    if (account.id === trip.account.id) {
      return true;
    }

    const guardians = await this.guardianService.getGuardiansByAccount({ account: trip.account });

    return guardians.some((guardian) => account.id === guardian.guardian.id);
  }

  async canViewDestination(account: IAccount, destination: ITripDestination): Promise<boolean> {
    return account.id === destination.account.id;
  }

  async canFavouriteDestination(account: IAccount, destination: ITripDestination): Promise<boolean> {
    return account.id === destination.account.id;
  }
}
