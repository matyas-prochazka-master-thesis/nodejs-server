import { Field, ObjectType } from '@nestjs/graphql';
import { Account } from 'src/modules/account/entities';

@ObjectType()
export class AuthReturnDto {
  @Field(() => Account)
  account: Account;

  @Field()
  accessToken: string;

  @Field()
  refreshToken: string;
}
