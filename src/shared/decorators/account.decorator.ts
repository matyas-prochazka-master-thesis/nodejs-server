import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IAccount } from '../../modules/account/interfaces';

export const Account = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): IAccount => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);