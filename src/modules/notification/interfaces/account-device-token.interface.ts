import { IAccount } from '../../account/interfaces';

export interface IAccountDeviceToken {
  id: number;

  account: IAccount;

  deviceToken: string;

  addedAt: Date;

  updatedAt: Date;
}
