import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as argon2 from 'argon2';
import { Repository } from 'typeorm';

import { ForbiddenException } from '../../../shared/exceptions';
import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { AccountService } from '../../account/services';
import { MailTemplates } from '../../mail/mail.constants';
import { MailService } from '../../mail/services';
import { ForgottenPasswordDto, LoginLocalDto, PasswordEditDto, PasswordResetDto } from '../dto';
import { PasswordReset } from '../entities';
import { ForgottenPasswordSourcesEnum, JwtTypeEnum } from '../enums';
import authExceptions from '../exceptions/auth-exceptions';

@Injectable()
export class AuthService {
  constructor(
    private accountService: AccountService,
    @InjectRepository(Account)
    private accountRepository: Repository<Account>,
    @InjectRepository(PasswordReset)
    private passwordResetRepository: Repository<PasswordReset>,
    private jwtService: JwtService,
    private mailService: MailService,
  ) {}

  async validateAccount(loginLocalDto: LoginLocalDto) {
    const { email, password } = loginLocalDto;

    const account = await this.accountService.findOneByEmail({ email });

    if (await argon2.verify(account.password, password)) {
      account.password = undefined;

      return account;
    }

    return null;
  }

  async createToken(account: IAccount): Promise<any> {
    const payload = {
      email: account.email,
      sub: account.id,
      role: account.role,
    };

    return {
      accessToken: this.jwtService.sign(
        {
          ...payload,
          type: JwtTypeEnum.ACCESS,
        },
        { expiresIn: '120m' },
      ),
      refreshToken: this.jwtService.sign(
        {
          ...payload,
          type: JwtTypeEnum.REFRESH,
        },
        { expiresIn: '60d' },
      ),
      account,
    };
  }

  // Not used right now
  async editPassword(account: IAccount, { password, oldPassword }: PasswordEditDto): Promise<void> {
    if (!(await argon2.verify(account.password, oldPassword))) {
      throw authExceptions.createInvalidOldPassword();
    }

    await account.setPassword(password);

    await this.accountRepository.save(account);
  }

  // Not used right now
  async forgottenPasswordEmail(forgottenPasswordDto: ForgottenPasswordDto): Promise<void> {
    const { email, source } = forgottenPasswordDto;

    const account = await this.accountService.findOneByEmail({ email });

    await this.passwordResetRepository.delete({ account });

    const code = await this.generateCode();
    await this.passwordResetRepository.save(new PasswordReset({ account, code }));

    return this.sendForgottenPasswordEmail({ email, code, source });
  }

  // Not used right now
  async sendForgottenPasswordEmail({
    email,
    code,
    source,
  }: {
    email: string;
    code: string;
    source: ForgottenPasswordSourcesEnum;
  }): Promise<void> {
    let template = MailTemplates.PASSWORD_FORGET;
    const link = ``;

    if ([ForgottenPasswordSourcesEnum.ANDROID_APP, ForgottenPasswordSourcesEnum.IOS_APP].includes(source)) {
      template = MailTemplates.PASSWORD_FORGET_APP;
    }

    await this.mailService.send({
      to: email,
      subject: 'Zapomenuté heslo',
      template,
      'v:code': code,
      'v:email': email,
      'v:link': link,
    });
  }

  // Not used right now
  async resetPassword(passwordResetDto: PasswordResetDto): Promise<void> {
    const { code, email, password } = passwordResetDto;

    const account = await this.accountService.findOneByEmail({ email });
    const passwordReset = await this.passwordResetRepository.findOne({ account, code });

    if (!passwordReset) {
      throw new ForbiddenException({ message: 'INVALID_PASSWORD_RESET_CODE' });
    } else if (passwordReset.expiresAt < new Date()) {
      throw new ForbiddenException({ message: 'INVALID_PASSWORD_RESET_CODE' });
    }

    await account.setPassword(password);
    await this.accountRepository.save(account);

    await this.passwordResetRepository.delete({ id: passwordReset.id });
  }

  async generateCode(): Promise<string> {
    // generate 10 characters long random code
    const characters = '123456789ABCDEFGHJKLMNPQRSTVUWXYZ';
    const len = characters.length;

    let code = '';

    for (let i = 0; i < 10; i += 1) {
      code += characters[Math.floor(Math.random() * len)];
    }

    return code;
  }
}
