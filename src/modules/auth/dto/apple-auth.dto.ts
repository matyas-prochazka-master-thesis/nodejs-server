import { Field, InputType } from '@nestjs/graphql';

import { AccountSourceEnum } from '../../account/enums';

@InputType()
export class AppleAuthDto {
  @Field()
  readonly code: string;

  @Field(() => AccountSourceEnum, { nullable: true })
  readonly source: AccountSourceEnum | null;
}
