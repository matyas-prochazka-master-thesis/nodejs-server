import { IFile } from '../../storage/interfaces';
import { TripActivityKeyEnum } from '../enums/trip-activity-key.enum';
import { ITrip } from './trip.interface';

export interface ITripActivity {
  id: number;

  trip: ITrip;

  key: TripActivityKeyEnum;

  latitude: number | null;
  longitude: number | null;
  batteryLevel: number | null;

  payload: string;
  attachment: IFile | null;

  createdAt: Date;
}
