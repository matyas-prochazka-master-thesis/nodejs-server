import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { RegisterLocalDto } from '../../auth/dto';
import { FolderPrefixEnum } from '../../storage/enums';
import { StorageService } from '../../storage/services';
import { AccountUpdateDto, ProfilePictureUpdateDto, UsernameUpdateDto } from '../dto';
import { Account } from '../entities';
import accountExceptions from '../exceptions/account-exceptions';
import { IAccount } from '../interfaces';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: Repository<Account>,
    private readonly storageService: StorageService,
  ) {}

  async find({ accountId }: { accountId: number }): Promise<IAccount> {
    const account = await this.accountRepository.findOne(accountId, {
      relations: ['profilePicture'],
    });

    if (!account) {
      throw accountExceptions.createAccountNotFound();
    }

    return account;
  }

  async findOneByEmail({ email }: { email: string }): Promise<IAccount> {
    const account = await this.accountRepository.findOne({ email: email.toLowerCase() });

    if (!account) {
      throw accountExceptions.createAccountNotFound();
    }

    return account;
  }

  async findOneByUsername({ username }: { username: string }): Promise<IAccount> {
    const account = await this.accountRepository.findOne({ username: username.toLowerCase() });

    if (!account) {
      throw accountExceptions.createAccountNotFound();
    }

    return account;
  }

  async create({ registerLocalDto }: { registerLocalDto: RegisterLocalDto }): Promise<IAccount> {
    let { email } = registerLocalDto;
    const { password, source } = registerLocalDto;

    email = email.toLowerCase();

    const exists = await this.accountRepository.count({ email });

    if (exists > 0) {
      throw accountExceptions.createEmailMustBeUnique();
    }

    const account = new Account({ email, source });

    await account.setPassword(password);

    await this.accountRepository.insert(account);

    return account;
  }

  async updateUsername({
    account,
    usernameUpdateDto,
  }: {
    account: IAccount;
    usernameUpdateDto: UsernameUpdateDto;
  }): Promise<IAccount> {
    let { username } = usernameUpdateDto;

    username = username.toLocaleLowerCase();

    const exists = await this.accountRepository.count({ username });

    if (exists > 0) {
      throw accountExceptions.createUsernameMustBeUnique();
    }

    account.username = username;

    await this.accountRepository.save(account);

    return account;
  }

  async updateAccount({
    account,
    accountUpdateDto,
  }: {
    account: IAccount;
    accountUpdateDto: AccountUpdateDto;
  }): Promise<IAccount> {
    const { name, telephone } = accountUpdateDto;
    let { username } = accountUpdateDto;

    username = username.toLocaleLowerCase();

    const exists = await this.accountRepository.count({ username });

    if (exists > 0) {
      throw accountExceptions.createUsernameMustBeUnique();
    }

    account.username = username;
    account.name = name;
    account.telephone = telephone;

    await this.accountRepository.save(account);

    return account;
  }

  async updateProfilePicture({
    account,
    profilePictureUpdateDto,
  }: {
    account: IAccount;
    profilePictureUpdateDto: ProfilePictureUpdateDto;
  }): Promise<IAccount> {
    const { profilePictureBase64 } = profilePictureUpdateDto;

    const loadedAccount = await this.find({ accountId: account.id });

    const oldProfilePicture = loadedAccount.profilePicture;

    // Delete old picture
    if (oldProfilePicture) {
      loadedAccount.profilePicture = null;

      await this.accountRepository.save(loadedAccount);
      await this.storageService.remove(oldProfilePicture);
    }

    loadedAccount.profilePicture = await this.storageService.uploadBase64(
      account,
      profilePictureBase64,
      FolderPrefixEnum.PROFILE_PICTURE,
    );

    await this.accountRepository.save(loadedAccount);

    return loadedAccount;
  }
}
