import { IAccount } from '../../account/interfaces';
import { CheckinRequestTypeEnum, CheckinRequestStateEnum } from '../enums';
import { ICheckinRequestGuardian } from './checkin-request-guardian.interface';
import { ICheckin } from './checkin.interface';

export interface ICheckinRequest {
  id: number;

  requester: IAccount;
  targetAccount: IAccount;

  type: CheckinRequestTypeEnum;
  state: CheckinRequestStateEnum;

  startsAt: Date | null;
  endsAt: Date | null;
  periodInMinutes: number | null;
  nextCheckinAt: Date | null;

  createdAt: Date;

  guardians: ICheckinRequestGuardian[];
  checkins: ICheckin[];
}
