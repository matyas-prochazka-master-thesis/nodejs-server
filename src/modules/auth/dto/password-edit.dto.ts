import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class PasswordEditDto {
  @Field()
  password: string;

  @Field()
  oldPassword: string;
}
