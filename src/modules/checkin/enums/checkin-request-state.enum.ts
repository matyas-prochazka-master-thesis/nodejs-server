import { registerEnumType } from '@nestjs/graphql';

export enum CheckinRequestStateEnum {
  REQUESTED = 'requested',
  ENDED = 'ended',
}

registerEnumType(CheckinRequestStateEnum, { name: 'CheckinRequestState' });
