import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { FirebaseService } from '../../firebase/services';
import { AccountDeviceToken } from '../entities';
import { INotification } from '../interfaces';

@Injectable()
export class NotifyService {
  constructor(
    @InjectRepository(AccountDeviceToken)
    private accountDeviceTokenRepository: Repository<AccountDeviceToken>,
    private readonly firebaseService: FirebaseService,
  ) {}

  async notifyAccount({ account, notification }: { account: IAccount; notification: INotification }): Promise<void> {
    const accountDeviceTokens = await this.accountDeviceTokenRepository.find({
      account,
    });

    if (accountDeviceTokens.length === 0) {
      return;
    }

    const deviceTokens = accountDeviceTokens.map((accountDeviceToken) => accountDeviceToken.deviceToken);

    const { title, body, payload } = notification;

    await this.firebaseService.sendNotificationForDevices({ title, body }, payload, deviceTokens);
  }
}
