import { TypeOrmModuleOptions } from '@nestjs/typeorm';

require('dotenv').config();
require('dotenv').config({
  path: `${__dirname}/../../.env.${process.env.MODE.toLowerCase()}`,
});

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: string, throwOnMissing = true): string {
    const value = this.env[key];

    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  public ensureValues(keys: string[]) {
    keys.forEach((k) => this.getValue(k, true));

    return this;
  }

  // JWT Secret
  public getJwt() {
    return {
      secret: this.getValue('JWT_SECRET', true),
    };
  }

  // Type of mailer
  public getMailer() {
    return this.getValue('MAILER', false);
  }

  // Mail from
  public getMailFrom() {
    return this.getValue('MAIL_FROM', true);
  }

  public getMailSupport() {
    return this.getValue('MAIL_SUPPORT', true);
  }

  // Mailgun configuration
  public getMailgun() {
    return {
      username: this.getValue('MAILGUN_USERNAME', true),
      key: this.getValue('MAILGUN_KEY', true),
      url: this.getValue('MAILGUN_URL', true),
      domain: this.getValue('MAILGUN_DOMAIN', true),
    };
  }

  // Type of storage
  public getStorage() {
    return this.getValue('STORAGE', false);
  }

  // S3 configuration
  public getS3() {
    return {
      accessKeyId: this.getValue('S3_ACCESS_KEY_ID', true),
      secretAccessKey: this.getValue('S3_SECRET_ACCESS_KEY', true),
      region: this.getValue('S3_REGION', true),
      bucket: this.getValue('S3_BUCKET', true),
      version: this.getValue('S3_VERSION', true),
      prefix: this.getValue('S3_PREFIX', false) || '',
    };
  }

  // Public storage url
  public getStorageUrl() {
    return this.getValue('STORAGE_URL', true);
  }

  // Server port
  public getPort() {
    return parseInt(this.getValue('PORT', true), 10);
  }

  public isProduction() {
    return this.getValue('MODE', false) == 'PROD';
  }

  public isTesting() {
    return this.getValue('MODE', false) == 'TEST';
  }

  // TypeORM configuration
  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',
      url: this.getValue('DATABASE_URL'),
      ssl:
        this.isProduction() || !!parseInt(this.getValue('DATABASE_SSL', false)) ? { rejectUnauthorized: false } : false,
      logging: !!parseInt(this.getValue('DATABASE_LOGGING', false)),
      synchronize: false,
      entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
      migrationsTableName: 'migration',
      migrations: [`${__dirname}/../migrations/*{.ts,.js}`],
      cli: {
        migrationsDir: 'src/migrations',
      },
    };
  }

  // Enable graphql playground and schema in production
  public getGqlIntrospection(): boolean {
    return this.getValue('GQL_INTROSPECTION', false) === '1';
  }

  // Google configuration
  public getGoogleClientId() {
    return this.getValue('GOOGLE_CLIENT_ID', true);
  }

  // Apple configuration
  public getAppleConfig() {
    return {
      client_id: this.getValue('APPLE_APP_ID', true),
      team_id: this.getValue('APPLE_TEAM_ID', true),
      key_id: this.getValue('APPLE_KEY_ID', true),
      key: this.getValue('APPLE_KEY', true),
      scope: 'email',
      redirect_uri: 'https://google.com',
    };
  }

  // Firebase configuration
  public getFirebaseConfig() {
    return {
      projectId: this.getValue('FIREBASE_PROJECT_ID'),
      privateKey: this.getValue('FIREBASE_PRIVATE_KEY').replace(/\\n/g, '\n'),
      clientEmail: this.getValue('FIREBASE_CLIENT_EMAIL'),
    };
  }

  public getFirebaseKey(): string {
    return this.getValue('FIREBASE_KEY', true);
  }

  // App configuration
  public getAppConfig() {
    return {
      sosEmergencyDelay: Number.parseInt(this.getValue('SOS_DELAY', false) ?? '10', 10),
    };
  }
}

const configService = new ConfigService(process.env).ensureValues(['MODE', 'JWT_SECRET', 'DATABASE_URL']);

export { configService };
