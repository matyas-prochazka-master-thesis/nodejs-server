import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Not, Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { AccountService } from '../../account/services';
import { INotification } from '../../notification/interfaces';
import { NotifyService } from '../../notification/services';
import { GuardianAttributes } from '../../voter/attributes';
import { VoterService } from '../../voter/voter.service';
import { GuardianAddDto, GuardianDeclineDto, GuardianRemoveDto, GuardianAcceptDto } from '../dto';
import { Guardian } from '../entities';
import guardianExceptions from '../exceptions/guardian-exceptions';
import { IGuardian } from '../interfaces';
import guardianNotifications from '../notifications/guardian-notifications';

@Injectable()
export class GuardianService {
  constructor(
    @InjectRepository(Guardian)
    private readonly guardianRepository: Repository<Guardian>,
    private readonly accountService: AccountService,
    private readonly voterService: VoterService,
    private readonly notifyService: NotifyService,
  ) {}

  async find({ guardianId }: { guardianId: number }): Promise<IGuardian> {
    const guardian = await this.guardianRepository.findOne(guardianId, {
      relations: ['account', 'guardian'],
    });

    if (!guardian) {
      throw guardianExceptions.createGuardianNotFound();
    }

    return guardian;
  }

  async getGuardiansByAccount({ account }: { account: IAccount }): Promise<IGuardian[]> {
    return this.guardianRepository.find({
      relations: ['guardian', 'guardian.profilePicture'],
      where: {
        account,
        acceptedAt: Not(IsNull()),
      },
    });
  }

  async getGuardiansRequests({ account }: { account: IAccount }): Promise<IGuardian[]> {
    return this.guardianRepository.find({
      relations: ['account', 'account.profilePicture'],
      where: {
        guardian: account,
        acceptedAt: IsNull(),
      },
    });
  }

  async acceptGuardianShip({
    account,
    guardianAcceptDto,
  }: {
    account: IAccount;
    guardianAcceptDto: GuardianAcceptDto;
  }): Promise<void> {
    const { guardianId } = guardianAcceptDto;

    const guardian = await this.find({ guardianId });

    if (!(await this.voterService.isGranted(GuardianAttributes.ACCEPT_GUARDIAN, guardian, account))) {
      throw guardianExceptions.createCannotAcceptGuardian();
    }

    // By accepting guardian request the requester will also become my guardian
    let reversedGuardian: Guardian | null = null;

    if (guardian.guardian.id !== guardian.account.id) {
      reversedGuardian = new Guardian({ account: guardian.guardian, guardian: guardian.account });

      reversedGuardian.acceptedAt = new Date();
    }

    // The acceptance is marked by acceptedAt not being NULL
    guardian.acceptedAt = new Date();

    await this.guardianRepository.save([guardian, reversedGuardian].filter(Boolean));

    const notification = guardianNotifications.createGuardianRequestAccepted({ account });

    // No need to await the notification request
    this.notifyService.notifyAccount({
      account: guardian.account,
      notification,
    });
  }

  async declineGuardianShip({
    account,
    guardianDeclineDto,
  }: {
    account: IAccount;
    guardianDeclineDto: GuardianDeclineDto;
  }): Promise<void> {
    const { guardianId } = guardianDeclineDto;

    const guardian = await this.find({ guardianId });

    if (!(await this.voterService.isGranted(GuardianAttributes.ACCEPT_GUARDIAN, guardian, account))) {
      throw guardianExceptions.createCannotDeclineGuardian();
    }

    await this.guardianRepository.remove(guardian);

    const notification = guardianNotifications.createGuardianRequestDeclined({ account });

    // No need to await the notification request
    this.notifyService.notifyAccount({
      account: guardian.account,
      notification,
    });
  }

  // Guardian request
  async addGuardian({ account, guardianAddDto }: { account: IAccount; guardianAddDto: GuardianAddDto }): Promise<void> {
    const { username } = guardianAddDto;

    const guardian = await this.accountService.findOneByUsername({ username });

    const alreadyExists = await this.guardianRepository.count({
      where: {
        account,
        guardian,
      },
    });

    if (alreadyExists > 0) {
      throw guardianExceptions.createAlreadyYourGuardian();
    }

    await this.guardianRepository.save(new Guardian({ account, guardian }));

    const notification = guardianNotifications.createGuardianRequest({ account });

    // No need to await the notification request
    this.notifyService.notifyAccount({
      account: guardian,
      notification,
    });
  }

  async removeGuardian({
    account,
    guardianRemoveDto,
  }: {
    account: IAccount;
    guardianRemoveDto: GuardianRemoveDto;
  }): Promise<void> {
    const { guardianId } = guardianRemoveDto;

    const guardian = await this.find({ guardianId });

    if (!(await this.voterService.isGranted(GuardianAttributes.REMOVE_GUARDIAN, guardian, account))) {
      throw guardianExceptions.createCannotRemoveGuardian();
    }

    // Guardian is a two-way thing so we need to remove the other entity as well
    const reversedGuardian = await this.guardianRepository.findOne({
      where: {
        account: guardian.guardian,
        guardian: account,
      },
    });

    await this.guardianRepository.remove([guardian, reversedGuardian].filter(Boolean));

    const notification = guardianNotifications.createGuardianRemoved({ account });

    // No need to await the notification request
    this.notifyService.notifyAccount({
      account: guardian.guardian,
      notification,
    });
  }

  async notifyGuardians({ account, notification }: { account: IAccount; notification: INotification }): Promise<void> {
    const guardians = await this.getGuardiansByAccount({ account });

    // No need to await the notification request
    guardians.forEach((guardian) => this.notifyService.notifyAccount({ account: guardian.guardian, notification }));
  }
}
