import { IAccount } from '../../account/interfaces';

export interface ITripDestination {
  id: number;

  account: IAccount;

  name: string;

  latitude: number;
  longitude: number;

  favourite: boolean;

  createdAt: Date;
}
