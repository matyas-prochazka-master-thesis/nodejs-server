import { Injectable } from '@nestjs/common';
import * as Sentry from '@sentry/node';
import formData from 'form-data';
import Mailgun from 'mailgun.js';

import { configService } from '../../../config/config.service';
import { InternalErrorException } from '../../../shared/exceptions';

@Injectable()
export class MailService {
  private readonly mailer;

  private readonly client;

  private readonly config;

  constructor() {
    this.mailer = configService.getMailer();

    if (this.mailer === 'mailgun') {
      this.config = {
        ...configService.getMailgun(),
        from: configService.getMailFrom(),
      };

      const mailgun = new Mailgun(formData);

      this.client = mailgun.client(this.config);
    }
  }

  async send(data: any): Promise<void> {
    if (this.mailer == 'mailgun') {
      await this.sendMailgun(data);
    }
  }

  async sendMailgun(data: any): Promise<void> {
    await this.client.messages
      .create(this.config.domain, {
        from: this.config.from,
        ...data,
      })
      .catch((error) => {
        Sentry.captureException(error);

        throw new InternalErrorException({ message: 'Problem with mail sending' });
      });
  }
}
