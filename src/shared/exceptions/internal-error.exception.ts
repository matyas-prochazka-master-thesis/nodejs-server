import { HttpStatus } from '@nestjs/common';

import { BaseException, BaseExceptionError } from './base.exception';

export class InternalErrorException extends BaseException {
  constructor(errors: BaseExceptionError | BaseExceptionError[]) {
    super('Internal Server Error', HttpStatus.INTERNAL_SERVER_ERROR, errors);
  }
}
