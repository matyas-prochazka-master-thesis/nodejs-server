import { MigrationInterface, QueryRunner } from 'typeorm';

export class checkin1648899292025 implements MigrationInterface {
  name = 'checkin1648899292025';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "checkin" ("id" SERIAL NOT NULL, "state" character varying NOT NULL, "latitude" numeric, "longitude" numeric, "checkedAt" TIMESTAMP WITH TIME ZONE, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "requestId" integer, CONSTRAINT "PK_ceddab0cc0235c228a841bac49e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "checkin_request" ("id" SERIAL NOT NULL, "type" character varying NOT NULL, "state" character varying NOT NULL, "endsAt" TIMESTAMP WITH TIME ZONE, "periodInMinutes" integer, "nextCheckinAt" TIMESTAMP WITH TIME ZONE, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "requesterId" integer, "targetAccountId" integer, CONSTRAINT "PK_c551bd7bfd6d99d4dcec99dba20" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "checkin_request_guardian" ("id" SERIAL NOT NULL, "requestId" integer, "guardianId" integer, CONSTRAINT "PK_c2317e218d4c0b8c221b40260f1" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "checkin" ADD CONSTRAINT "FK_e1b0839c0c1098f141fabc77ffc" FOREIGN KEY ("requestId") REFERENCES "checkin_request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "checkin_request" ADD CONSTRAINT "FK_cef5f3bc1d6d6553be390e55af7" FOREIGN KEY ("requesterId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "checkin_request" ADD CONSTRAINT "FK_907d31a67562001ec72e56b8c8d" FOREIGN KEY ("targetAccountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "checkin_request_guardian" ADD CONSTRAINT "FK_f92a6780fb824b065a62ce54e09" FOREIGN KEY ("requestId") REFERENCES "checkin_request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "checkin_request_guardian" ADD CONSTRAINT "FK_c343e457157fbf3c8806b1230de" FOREIGN KEY ("guardianId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "checkin_request_guardian" DROP CONSTRAINT "FK_c343e457157fbf3c8806b1230de"`);
    await queryRunner.query(`ALTER TABLE "checkin_request_guardian" DROP CONSTRAINT "FK_f92a6780fb824b065a62ce54e09"`);
    await queryRunner.query(`ALTER TABLE "checkin_request" DROP CONSTRAINT "FK_907d31a67562001ec72e56b8c8d"`);
    await queryRunner.query(`ALTER TABLE "checkin_request" DROP CONSTRAINT "FK_cef5f3bc1d6d6553be390e55af7"`);
    await queryRunner.query(`ALTER TABLE "checkin" DROP CONSTRAINT "FK_e1b0839c0c1098f141fabc77ffc"`);
    await queryRunner.query(`DROP TABLE "checkin_request_guardian"`);
    await queryRunner.query(`DROP TABLE "checkin_request"`);
    await queryRunner.query(`DROP TABLE "checkin"`);
  }
}
