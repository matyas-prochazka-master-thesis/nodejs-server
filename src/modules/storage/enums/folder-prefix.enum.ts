export enum FolderPrefixEnum {
  PROFILE_PICTURE = 'profile-picture/',
  TRIP = 'trip/',
}
