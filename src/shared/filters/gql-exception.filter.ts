import { Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { GqlExceptionFilter } from '@nestjs/graphql';

import { BaseException, InternalErrorException } from '../exceptions';

@Catch()
export class GqlFilter implements GqlExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    console.log(exception);

    // If it is not an app exception (so it is from another library etc.) throw basic InternalError
    if (!(exception instanceof BaseException) && !(exception instanceof HttpException)) {
      return new InternalErrorException({ message: 'Internal server error' });
    }

    return exception;
  }
}
