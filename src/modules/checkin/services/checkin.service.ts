import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { addHours } from 'date-fns';
import { Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { AccountService } from '../../account/services';
import { GuardianService } from '../../guardian/services';
import { INotification } from '../../notification/interfaces';
import { NotifyService } from '../../notification/services';
import { CheckinAttributes } from '../../voter/attributes';
import { VoterService } from '../../voter/voter.service';
import { CheckinRequestDto, CheckinRequestPeriodicalDto, CheckinDto } from '../dto';
import { Checkin, CheckinRequest, CheckinRequestGuardian } from '../entities';
import { CheckinRequestStateEnum, CheckinRequestTypeEnum, CheckinStateEnum } from '../enums';
import checkinExceptions from '../exceptions/checkin-exceptions';
import { ICheckin, ICheckinRequest, ICheckinRequestGuardian } from '../interfaces';
import checkinNotifications from '../notifications/checkin-notifications';

@Injectable()
export class CheckinService {
  constructor(
    @InjectRepository(Checkin)
    private readonly checkinRepository: Repository<Checkin>,
    @InjectRepository(CheckinRequest)
    private readonly checkinRequestRepository: Repository<CheckinRequest>,
    @InjectRepository(CheckinRequestGuardian)
    private readonly checkinRequestGuardianRepository: Repository<CheckinRequestGuardian>,
    private readonly guardianService: GuardianService,
    private readonly accountService: AccountService,
    private readonly voterService: VoterService,
    private readonly notifyService: NotifyService,
  ) {}

  async findCheckinRequest({ checkinRequestId }: { checkinRequestId: number }): Promise<ICheckinRequest> {
    const request = await this.checkinRequestRepository.findOne(checkinRequestId, {
      relations: ['targetAccount', 'requester', 'guardians', 'guardians.account'],
    });

    if (!request) {
      throw checkinExceptions.createCheckinRequestNotFound();
    }

    return request;
  }

  async findCheckinRequestWithAllInfo({ checkinRequestId }: { checkinRequestId: number }): Promise<ICheckinRequest> {
    const request = await this.checkinRequestRepository.findOne(checkinRequestId, {
      relations: [
        'targetAccount',
        'targetAccount.profilePicture',
        'requester',
        'requester.profilePicture',
        'guardians',
        'guardians.account',
        'guardians.account.profilePicture',
      ],
    });

    if (!request) {
      throw checkinExceptions.createCheckinRequestNotFound();
    }

    return request;
  }

  async findCheckin({ checkinId }: { checkinId: number }): Promise<ICheckin> {
    const checkin = await this.checkinRepository.findOne(checkinId, {
      relations: ['request', 'request.targetAccount', 'request.requester'],
    });

    if (!checkin) {
      throw checkinExceptions.createCheckinNotFound();
    }

    return checkin;
  }

  async getCheckinRequest({
    account,
    checkinRequestId,
  }: {
    account: IAccount;
    checkinRequestId: number;
  }): Promise<ICheckinRequest> {
    const request = await this.findCheckinRequestWithAllInfo({ checkinRequestId });

    if (!(await this.voterService.isGranted(CheckinAttributes.CHECKIN_REQUEST_VIEW, request, account))) {
      throw checkinExceptions.createCheckinRequestCannotView();
    }

    return request;
  }

  async getRequestGuardians({ request }: { request: ICheckinRequest }): Promise<ICheckinRequestGuardian[]> {
    return this.checkinRequestGuardianRepository.find({
      relations: ['account'],
      where: {
        request,
      },
    });
  }

  async getRequestsForAccount({ account }: { account: IAccount }): Promise<ICheckinRequest[]> {
    return this.checkinRequestRepository.find({
      relations: [
        'requester',
        'requester.profilePicture',
        'guardians',
        'guardians.account',
        'guardians.account.profilePicture',
      ],
      where: {
        targetAccount: account,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async getRequestsAccountFollows({ account }: { account: IAccount }): Promise<ICheckinRequest[]> {
    return (
      this.checkinRequestRepository
        .createQueryBuilder('request')
        .leftJoin('request.guardians', 'guardian')
        .leftJoinAndSelect('request.targetAccount', 'targetAccount')
        .leftJoinAndSelect('targetAccount.profilePicture', 'profilePicture')
        .where('guardian.account = :accountId', { accountId: account.id })
        // .orWhere('request.requester = :requesterId', { requesterId: account.id })
        .orderBy('request.createdAt', 'DESC')
        .getMany()
    );
  }

  async getCheckinsByRequest({
    account,
    checkinRequestId,
  }: {
    account: IAccount;
    checkinRequestId: number;
  }): Promise<ICheckin[]> {
    const request = await this.findCheckinRequest({ checkinRequestId });

    if (!(await this.voterService.isGranted(CheckinAttributes.CHECKIN_REQUEST_VIEW, request, account))) {
      throw checkinExceptions.createCheckinRequestCannotView();
    }

    return this.checkinRepository.find({
      where: {
        request,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async getUncheckedCheckinsForMe({ account }: { account: IAccount }): Promise<ICheckin[]> {
    return this.checkinRepository
      .createQueryBuilder('checkin')
      .leftJoinAndSelect('checkin.request', 'request')
      .leftJoinAndSelect('request.requester', 'requester')
      .leftJoinAndSelect('requester.profilePicture', 'profilePicture')
      .where('request.targetAccount.id = :targetAccountId', { targetAccountId: account.id })
      .andWhere('checkin.state = :state', { state: CheckinStateEnum.REQUESTED })
      .getMany();
  }

  // ONE-TIME Checkin
  async requestCheckin({
    account,
    checkinRequestDto,
  }: {
    account: IAccount;
    checkinRequestDto: CheckinRequestDto;
  }): Promise<void> {
    const { targetAccountId } = checkinRequestDto;

    const targetAccount = await this.accountService.find({ accountId: targetAccountId });

    if (!(await this.voterService.isGranted(CheckinAttributes.REQUEST_CHECKIN, targetAccount, account))) {
      throw checkinExceptions.createCannotRequest();
    }

    const request = await this.checkinRequestRepository.save(
      new CheckinRequest({
        requester: account,
        targetAccount,
        type: CheckinRequestTypeEnum.ONE_TIME,
        startsAt: null,
        endsAt: null,
        periodInMinutes: null,
        nextCheckinAt: new Date(),
      }),
    );

    // Add the requester as a guardian
    await this.addGuardiansToRequest({ request, guardianAccountIds: [account.id] });
  }

  // PERIODICAL Checkin
  async requestPeriodicalCheckin({
    account,
    checkinRequestPeriodicalDto,
  }: {
    account: IAccount;
    checkinRequestPeriodicalDto: CheckinRequestPeriodicalDto;
  }): Promise<void> {
    const { targetAccountId, startsAt, endsAt, periodInMinutes, guardianAccountIds } = checkinRequestPeriodicalDto;

    const targetAccount = await this.accountService.find({ accountId: targetAccountId });

    if (!(await this.voterService.isGranted(CheckinAttributes.REQUEST_CHECKIN, targetAccount, account))) {
      throw checkinExceptions.createCannotRequest();
    }

    if (startsAt >= endsAt) {
      throw checkinExceptions.createBadRangeInPeriodicalRequest();
    }

    if (addHours(startsAt, 12) < endsAt) {
      throw checkinExceptions.createBadRangeInPeriodicalRequest();
    }

    if (periodInMinutes < 5 || periodInMinutes > 120) {
      throw checkinExceptions.createPeriodInPeriodicalRequest();
    }

    const request = await this.checkinRequestRepository.save(
      new CheckinRequest({
        requester: account,
        targetAccount,
        type: CheckinRequestTypeEnum.PERIODICAL,
        startsAt,
        endsAt,
        periodInMinutes,
        nextCheckinAt: startsAt,
      }),
    );

    // If the requester is another person include him in guardians
    if (!guardianAccountIds.includes(account.id)) {
      guardianAccountIds.push(account.id);
    }

    await this.addGuardiansToRequest({ request, guardianAccountIds });

    // Notify the target account about the start of periodical checkin
    const notification = checkinNotifications.createStartedPeriodicalCheckin({ request });

    this.notifyService.notifyAccount({ account: targetAccount, notification });
  }

  async addGuardiansToRequest({
    request,
    guardianAccountIds,
  }: {
    request: ICheckinRequest;
    guardianAccountIds: number[];
  }): Promise<void> {
    for (const guardianAccountId of guardianAccountIds) {
      const guardianAccount = await this.accountService.find({ accountId: guardianAccountId });

      await this.checkinRequestGuardianRepository.save(
        new CheckinRequestGuardian({ request, account: guardianAccount }),
      );
    }
  }

  async cancelPeriodicalRequest({
    account,
    checkinRequestId,
  }: {
    account: IAccount;
    checkinRequestId: number;
  }): Promise<void> {
    const request = await this.findCheckinRequest({ checkinRequestId });

    if (!(await this.voterService.isGranted(CheckinAttributes.CHECKIN_REQUEST_UPDATE, request, account))) {
      throw checkinExceptions.createCheckinRequestCannotUpdate();
    }

    request.endsAt = new Date();
    request.nextCheckinAt = null;
    request.state = CheckinRequestStateEnum.ENDED;

    await this.checkinRequestRepository.save(request);
  }

  // CHECKIN
  async checkin({ account, checkinDto }: { account: IAccount; checkinDto: CheckinDto }): Promise<void> {
    const { checkinId, latitude, longitude } = checkinDto;

    const checkin = await this.findCheckin({ checkinId });

    if (!(await this.voterService.isGranted(CheckinAttributes.CHECKIN_CHECK, checkin, account))) {
      throw checkinExceptions.createCannotCheck();
    }

    checkin.state = CheckinStateEnum.CHECKED;
    checkin.checkedAt = new Date();
    checkin.latitude = latitude;
    checkin.longitude = longitude;

    await this.checkinRepository.save(checkin);

    const { request } = checkin;

    if (request.nextCheckinAt === null) {
      request.state = CheckinRequestStateEnum.ENDED;

      await this.checkinRequestRepository.save(request);
    }

    const notification = checkinNotifications.createCheckinCheck({ checkin });

    this.notifyRequestGuardians({ request: checkin.request, notification });
  }

  // NOTIFY GUARDIANS
  async notifyRequestGuardians({
    request,
    notification,
  }: {
    request: ICheckinRequest;
    notification: INotification;
  }): Promise<void> {
    const requestGuardians = await this.getRequestGuardians({ request });

    requestGuardians.forEach((requestGuardian) => {
      this.notifyService.notifyAccount({ account: requestGuardian.account, notification });
    });
  }
}
