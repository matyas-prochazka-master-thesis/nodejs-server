import { MigrationInterface, QueryRunner } from 'typeorm';

export class sosPositionOptional1648800180665 implements MigrationInterface {
  name = 'sosPositionOptional1648800180665';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sos" ALTER COLUMN "initialLatitude" DROP NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sos" ALTER COLUMN "initialLongitude" DROP NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sos" ALTER COLUMN "initialLongitude" SET NOT NULL`);
    await queryRunner.query(`ALTER TABLE "sos" ALTER COLUMN "initialLatitude" SET NOT NULL`);
  }
}
