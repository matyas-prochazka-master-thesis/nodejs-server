import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { File } from './entities';
import { StorageService } from './services';

@Module({
  imports: [TypeOrmModule.forFeature([File])],
  providers: [StorageService],
  controllers: [],
  exports: [StorageService],
})
export class StorageModule {}
