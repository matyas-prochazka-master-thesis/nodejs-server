import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

// REST get api token from authentication header
@Injectable()
export class AuthJwtGuard extends AuthGuard('jwt') {}
