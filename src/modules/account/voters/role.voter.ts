import { RoleAttributes } from '../../voter/attributes';
import { IVoter } from '../../voter/interfaces';
import { AccountRolesEnum } from '../enums';
import { IAccount } from '../interfaces';

export class RoleVoter extends IVoter {
  async supports(attribute: string): Promise<boolean> {
    return (<any>Object).values(RoleAttributes).includes(attribute);
  }

  async voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    switch (attribute) {
      case RoleAttributes.HAS_ROLE:
        return this.hasRole(account, subject);
    }

    return false;
  }

  async hasRole(account: IAccount, subject: AccountRolesEnum): Promise<boolean> {
    return account.role === subject;
  }
}
