import { Field, ID, InputType } from '@nestjs/graphql';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';

import { TripDestinationCreateDto } from './trip-destination-create.dto';
import { TripQuickActionDto } from './trip-quick-action.dto';

@InputType()
export class TripCreateDto {
  @Field({ nullable: true })
  readonly shouldArriveAt: Date | null;

  @Field(() => ID, { nullable: true })
  readonly tripDestinationId: number | null;

  @Field(() => TripDestinationCreateDto, { nullable: true })
  @Type(() => TripDestinationCreateDto)
  readonly tripDestinationCreateDto: TripDestinationCreateDto | null;

  @Field(() => [TripQuickActionDto])
  @Type(() => TripQuickActionDto)
  @ValidateNested({ each: true })
  @IsArray()
  readonly quickActions: TripQuickActionDto[];
}
