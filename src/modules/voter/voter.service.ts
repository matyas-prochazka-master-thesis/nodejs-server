import { Injectable } from '@nestjs/common';

import { IAccount } from '../account/interfaces';
import { IVoter } from './interfaces';

@Injectable()
export class VoterService {
  private voters: IVoter[] = [];

  async isGranted(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    for (const voter of this.voters) {
      if (await voter.isGranted(attribute, subject, account)) {
        return true;
      }
    }

    return false;
  }

  register(voter: IVoter): void {
    this.voters.push(voter);
  }
}
