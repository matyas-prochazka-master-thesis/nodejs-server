import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GuardianModule } from '../guardian/guardian.module';
import { Sos, SosPosition } from './entities';
import { SosResolver } from './resolvers';
import { SosEmergencyService, SosService } from './services';
import { SosVoter } from './voters/sos.voter';

@Module({
  imports: [TypeOrmModule.forFeature([Sos, SosPosition]), GuardianModule],
  providers: [SosService, SosEmergencyService, SosResolver, SosVoter],
  controllers: [],
  exports: [],
})
export class SosModule {}
