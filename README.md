# Safie Server

Tested for NodeJS 16

## Installation

```shell script
$ yarn install
```

- Copy vars shared across modes from `./.env.example` to `./.env`
- Copy MODE dependant vars to `./.env.dev`, `./.env.prod`, `./.env.test`
- !! Always have `MODE=XY` in `./env` file and change it in accordance to your goal.
  - `MODE=TEST` will firstly load `./.env` file and then `./.env.test` file and bear in mind that existing vars will NOT BE OVERRIDED. The first appearance is used.  

## Migrations

```shell script
# create
$ yarn migration:g name
```

```shell script
# migrate
$ yarn migration:r
```

## Running the app

```shell script
# build
$ yarn build

# development
$ yarn start

# watch mode (best for local development)
$ yarn start:dev

# production mode
$ yarn start:prod
```

## File structure

- Procfile - Heroku config
- Src - Source files
  - config
  - modules
  - scripts - common scripts
  - shared - exceptions, guard etc.
  - modules - the main logic
