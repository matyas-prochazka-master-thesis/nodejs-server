import { HttpStatus } from '@nestjs/common';

import { BaseException, BaseExceptionError } from './base.exception';

export class BadRequestException extends BaseException {
  constructor(errors: BaseExceptionError | BaseExceptionError[]) {
    super('Bad request', HttpStatus.BAD_REQUEST, errors);
  }
}
