import {MigrationInterface, QueryRunner} from "typeorm";

export class activityInfoOptional1649319242807 implements MigrationInterface {
    name = 'activityInfoOptional1649319242807'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_activity" ALTER COLUMN "latitude" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ALTER COLUMN "longitude" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ALTER COLUMN "batteryLevel" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_activity" ALTER COLUMN "batteryLevel" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ALTER COLUMN "longitude" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ALTER COLUMN "latitude" SET NOT NULL`);
    }

}
