import { Field, InputType } from '@nestjs/graphql';

import { AccountSourceEnum } from '../../account/enums';

@InputType()
export class GoogleAuthDto {
  @Field()
  readonly idToken: string;

  @Field(() => AccountSourceEnum, { nullable: true })
  readonly source: AccountSourceEnum | null;
}
