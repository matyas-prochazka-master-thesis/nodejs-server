import { MigrationInterface, QueryRunner } from 'typeorm';

export class sos1648301528726 implements MigrationInterface {
  name = 'sos1648301528726';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "sos" ("id" SERIAL NOT NULL, "idFromClient" character varying NOT NULL, "state" character varying NOT NULL, "buttonPressedAt" TIMESTAMP WITH TIME ZONE, "buttonReleasedAt" TIMESTAMP WITH TIME ZONE, "pinEnteredAt" TIMESTAMP WITH TIME ZONE, "guardiansContactedAt" TIMESTAMP WITH TIME ZONE, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "accountId" integer, CONSTRAINT "UQ_12e86914a68e0eaff7a085b5462" UNIQUE ("idFromClient"), CONSTRAINT "PK_eb329f400ba735af187e98faec6" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "sos" ADD CONSTRAINT "FK_5d8f5c3cbf626927ba9771b63da" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "sos" DROP CONSTRAINT "FK_5d8f5c3cbf626927ba9771b63da"`);
    await queryRunner.query(`DROP TABLE "sos"`);
  }
}
