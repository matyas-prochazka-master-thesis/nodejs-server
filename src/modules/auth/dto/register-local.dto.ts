import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsEmail } from 'class-validator';

import { AccountSourceEnum } from '../../account/enums';

@InputType()
export class RegisterLocalDto {
  @Field()
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @Field()
  @IsNotEmpty()
  readonly password: string;

  @Field(() => AccountSourceEnum, { nullable: true })
  readonly source: AccountSourceEnum | null;
}
