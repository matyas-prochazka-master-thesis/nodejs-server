import {MigrationInterface, QueryRunner} from "typeorm";

export class guardian1647525170709 implements MigrationInterface {
    name = 'guardian1647525170709'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "guardian" ("id" SERIAL NOT NULL, "accountId" integer, "guardianId" integer, CONSTRAINT "PK_5eb51ec9378bc6b07702717160e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "guardian" ADD CONSTRAINT "FK_a008c4dd438293c3ce461bb2064" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "guardian" ADD CONSTRAINT "FK_47a0232b8db5bec739d357c8222" FOREIGN KEY ("guardianId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "guardian" DROP CONSTRAINT "FK_47a0232b8db5bec739d357c8222"`);
        await queryRunner.query(`ALTER TABLE "guardian" DROP CONSTRAINT "FK_a008c4dd438293c3ce461bb2064"`);
        await queryRunner.query(`DROP TABLE "guardian"`);
    }

}
