export interface INotification {
  title: string;

  body: string;

  payload: any;
}
