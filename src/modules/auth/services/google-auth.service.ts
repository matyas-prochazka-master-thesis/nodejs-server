import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OAuth2Client } from 'google-auth-library';
import { Repository } from 'typeorm';

import { configService } from '../../../config/config.service';
import { Account } from '../../account/entities';
import { AccountSourceEnum } from '../../account/enums';
import { IAccount } from '../../account/interfaces';
import { AuthReturnDto, GoogleAuthDto } from '../dto';
import authExceptions from '../exceptions/auth-exceptions';
import { AuthService } from './auth.service';

@Injectable()
export class GoogleAuthService {
  private client: OAuth2Client;

  constructor(
    @InjectRepository(Account)
    private accountRepository: Repository<Account>,
    private authService: AuthService,
  ) {
    this.client = new OAuth2Client(configService.getGoogleClientId());
  }

  async auth({ googleAuthDto }: { googleAuthDto: GoogleAuthDto }): Promise<AuthReturnDto> {
    const { idToken, source } = googleAuthDto;

    const ticket = await this.client.verifyIdToken({
      idToken,
      audience: configService.getGoogleClientId(),
    });

    const payload = ticket.getPayload();

    if (!payload) {
      throw authExceptions.createInvalidGoogleAuthToken();
    }

    return this.authService.createToken(
      await this.getOrCreate({ email: payload.email.toLowerCase(), googleId: payload.sub, source }),
    );
  }

  private async getOrCreate({
    googleId,
    email,
    source,
  }: {
    googleId: string;
    email: string;
    source: AccountSourceEnum | null;
  }): Promise<IAccount> {
    let account = await this.accountRepository.findOne({ googleId });

    if (account) {
      return account;
    }

    account = await this.accountRepository.findOne({ email });
    if (!account) {
      account = new Account({ email, source });
    }

    account.googleId = googleId;

    return this.accountRepository.save(account);
  }
}
