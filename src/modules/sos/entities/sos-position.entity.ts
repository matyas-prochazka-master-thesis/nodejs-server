import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ISos, ISosPosition } from '../interfaces';
import { Sos } from './sos.entity';

@ObjectType()
@Entity()
export class SosPosition implements ISosPosition {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Sos)
  @ManyToOne(() => Sos)
  sos: ISos;

  @Field()
  @Column('decimal')
  latitude: number;

  @Field()
  @Column('decimal')
  longitude: number;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  constructor(props: { sos: ISos; latitude: number; longitude: number }) {
    this.sos = props?.sos;
    this.latitude = props?.latitude;
    this.longitude = props?.longitude;
  }
}
