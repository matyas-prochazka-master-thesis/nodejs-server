import {MigrationInterface, QueryRunner} from "typeorm";

export class tripDestination1649330280211 implements MigrationInterface {
    name = 'tripDestination1649330280211'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "trip_destination" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "latitude" integer NOT NULL, "longitude" integer NOT NULL, "favourite" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "accountId" integer, CONSTRAINT "PK_92d5315d2e5e265c08324bb615a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "trip" ADD "shouldArriveAt" TIMESTAMP WITH TIME ZONE`);
        await queryRunner.query(`ALTER TABLE "trip" ADD "destinationId" integer`);
        await queryRunner.query(`ALTER TABLE "trip_destination" ADD CONSTRAINT "FK_a045512c52e782aa4dbe43c0b76" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "trip" ADD CONSTRAINT "FK_e0152523c65e8b32e04e59d04b2" FOREIGN KEY ("destinationId") REFERENCES "trip_destination"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip" DROP CONSTRAINT "FK_e0152523c65e8b32e04e59d04b2"`);
        await queryRunner.query(`ALTER TABLE "trip_destination" DROP CONSTRAINT "FK_a045512c52e782aa4dbe43c0b76"`);
        await queryRunner.query(`ALTER TABLE "trip" DROP COLUMN "destinationId"`);
        await queryRunner.query(`ALTER TABLE "trip" DROP COLUMN "shouldArriveAt"`);
        await queryRunner.query(`DROP TABLE "trip_destination"`);
    }

}
