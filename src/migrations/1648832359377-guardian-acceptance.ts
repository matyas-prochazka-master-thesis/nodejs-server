import { MigrationInterface, QueryRunner } from 'typeorm';

export class guardianAcceptance1648832359377 implements MigrationInterface {
  name = 'guardianAcceptance1648832359377';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "guardian" DROP COLUMN "addedAt"`);
    await queryRunner.query(`ALTER TABLE "guardian" ADD "acceptedAt" TIMESTAMP WITH TIME ZONE`);
    await queryRunner.query(`ALTER TABLE "guardian" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "guardian" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "guardian" DROP COLUMN "acceptedAt"`);
    await queryRunner.query(`ALTER TABLE "guardian" ADD "addedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`);
  }
}
