import { IAccount } from '../../account/interfaces';

const createGuardianRequest = ({ account }: { account: IAccount }) => {
  return {
    title: `${account.username ?? account.email} wants to add you as a guardian`,
    body: `Go to the app -> your profile -> guardians -> requests`,
    payload: {
      type: 'info',
      open: 'screen-guardian-requests',
      'localized-title-en': `${account.username ?? account.email} want to add you as a guardian`,
      'localized-body-en': `Go to the app -> you profile -> guardians -> requests`,
    },
  };
};

const createGuardianRequestAccepted = ({ account }: { account: IAccount }) => {
  return {
    title: `${account.username ?? account.email} accepted your guardianship`,
    body: `${account.username ?? account.email} is now your guardian`,
    payload: {
      type: 'success',
      'localized-title-en': `${account.username ?? account.email} accepted your guardianship`,
      'localized-body-en': `${account.username ?? account.email} is now your guardian`,
    },
  };
};

const createGuardianRequestDeclined = ({ account }: { account: IAccount }) => {
  return {
    title: `${account.username ?? account.email} declined your guardianship`,
    body: `${account.username ?? account.email} will not be your guardian`,
    payload: {
      type: 'success',
      'localized-title-en': `${account.username ?? account.email} declined your guardianship`,
      'localized-body-en': `${account.username ?? account.email} will not be your guardian`,
    },
  };
};

const createGuardianRemoved = ({ account }: { account: IAccount }) => {
  return {
    title: `${account.username ?? account.email} removed you as a guardian`,
    body: `You will no longer receive emergency alerts from him/her`,
    payload: {
      type: 'warning',
      'localized-title-en': `${account.username ?? account.email} removed you as a guardian`,
      'localized-body-en': `You will no longer receive emergency alerts from him/her`,
    },
  };
};

export default {
  createGuardianRequest,
  createGuardianRequestAccepted,
  createGuardianRequestDeclined,
  createGuardianRemoved,
};
