import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CheckinRequestPeriodicalDto {
  @Field(() => ID)
  @IsNotEmpty()
  readonly targetAccountId: number;

  @Field()
  @IsNotEmpty()
  readonly startsAt: Date;

  @Field()
  @IsNotEmpty()
  readonly endsAt: Date;

  @Field()
  @IsNotEmpty()
  readonly periodInMinutes: number;

  @Field(() => [ID])
  @IsNotEmpty()
  readonly guardianAccountIds: number[];
}
