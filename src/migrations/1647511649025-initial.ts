import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1647511649025 implements MigrationInterface {
    name = 'initial1647511649025'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "file" ("id" SERIAL NOT NULL, "path" character varying NOT NULL, "mimeType" character varying NOT NULL, "size" integer NOT NULL, "addedAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_36b46d232307066b3a2c9ea3a1d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "account" ("id" SERIAL NOT NULL, "username" character varying, "email" character varying NOT NULL, "password" character varying, "role" character varying NOT NULL DEFAULT 'registered', "source" character varying, "appleId" character varying, "facebookId" character varying, "googleId" character varying, "registeredAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "profilePictureId" integer, CONSTRAINT "UQ_41dfcb70af895ddf9a53094515b" UNIQUE ("username"), CONSTRAINT "UQ_7f6adb5ac85250b1f40b9c5d586" UNIQUE ("appleId"), CONSTRAINT "UQ_00fe236d9ea467d5702503e1d4b" UNIQUE ("facebookId"), CONSTRAINT "UQ_56859d94a71ad5b7bc5d6f84730" UNIQUE ("googleId"), CONSTRAINT "REL_dd5880d24866f0996aec258669" UNIQUE ("profilePictureId"), CONSTRAINT "PK_54115ee388cdb6d86bb4bf5b2ea" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "password_reset" ("id" SERIAL NOT NULL, "code" character varying NOT NULL, "expiresAt" TIMESTAMP NOT NULL, "accountId" integer, CONSTRAINT "REL_9722d75a3a379324a44b9d8951" UNIQUE ("accountId"), CONSTRAINT "PK_8515e60a2cc41584fa4784f52ce" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "account_device_token" ("id" SERIAL NOT NULL, "deviceToken" character varying NOT NULL, "addedAt" TIMESTAMP WITH TIME ZONE NOT NULL, "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL, "accountId" integer NOT NULL, CONSTRAINT "PK_18cafa3e1adb0cb44f6c2c8ce45" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "account" ADD CONSTRAINT "FK_dd5880d24866f0996aec2586695" FOREIGN KEY ("profilePictureId") REFERENCES "file"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "password_reset" ADD CONSTRAINT "FK_9722d75a3a379324a44b9d89514" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "account_device_token" ADD CONSTRAINT "FK_bb9142a1c30b3017c1cc7106280" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "account_device_token" DROP CONSTRAINT "FK_bb9142a1c30b3017c1cc7106280"`);
        await queryRunner.query(`ALTER TABLE "password_reset" DROP CONSTRAINT "FK_9722d75a3a379324a44b9d89514"`);
        await queryRunner.query(`ALTER TABLE "account" DROP CONSTRAINT "FK_dd5880d24866f0996aec2586695"`);
        await queryRunner.query(`DROP TABLE "account_device_token"`);
        await queryRunner.query(`DROP TABLE "password_reset"`);
        await queryRunner.query(`DROP TABLE "account"`);
        await queryRunner.query(`DROP TABLE "file"`);
    }

}
