import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

import { ForbiddenException } from '../../../shared/exceptions';
import { AuthService } from '../services';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    });
  }

  async validate(email: string, password: string): Promise<any> {
    const loginLocalDto = {
      email,
      password,
    };

    const account = await this.authService.validateAccount(loginLocalDto);

    if (!account) {
      throw new ForbiddenException({ message: 'Invalid credentials' });
    }

    return account;
  }
}
