import { registerEnumType } from '@nestjs/graphql';

export enum AccountRolesEnum {
  ROLE_REGISTERED = 'registered',
  ROLE_ADMIN = 'admin',
}

registerEnumType(AccountRolesEnum, { name: 'AccountRoles' });
