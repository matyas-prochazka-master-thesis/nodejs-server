import { MigrationInterface, QueryRunner } from 'typeorm';

export class checkinGuardian1648903431334 implements MigrationInterface {
  name = 'checkinGuardian1648903431334';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "checkin_request_guardian" DROP CONSTRAINT "FK_c343e457157fbf3c8806b1230de"`);
    await queryRunner.query(`ALTER TABLE "checkin_request_guardian" RENAME COLUMN "guardianId" TO "accountId"`);
    await queryRunner.query(
      `ALTER TABLE "checkin_request_guardian" ADD CONSTRAINT "FK_f47a35a60095f960d272ba24ad6" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "checkin_request_guardian" DROP CONSTRAINT "FK_f47a35a60095f960d272ba24ad6"`);
    await queryRunner.query(`ALTER TABLE "checkin_request_guardian" RENAME COLUMN "accountId" TO "guardianId"`);
    await queryRunner.query(
      `ALTER TABLE "checkin_request_guardian" ADD CONSTRAINT "FK_c343e457157fbf3c8806b1230de" FOREIGN KEY ("guardianId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
