import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class TripQuickActionDto {
  @Field()
  @IsNotEmpty()
  readonly id: string;

  @Field()
  @IsNotEmpty()
  readonly name: string;

  @Field()
  @IsNotEmpty()
  readonly icon: string;
}
