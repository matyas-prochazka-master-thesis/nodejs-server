import { ForbiddenException, NotFoundException } from '../../../shared/exceptions';

const createSosNotFound = () => {
  return new NotFoundException({ message: 'Sos not found' });
};

const createCannotUpdateSos = () => {
  return new ForbiddenException({ message: 'Cannot update SOS' });
};

const createCannotViewSos = () => {
  return new ForbiddenException({ message: 'Cannot view SOS' });
};

export default {
  createSosNotFound,
  createCannotUpdateSos,
  createCannotViewSos,
};
