import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { addMinutes } from 'date-fns';
import { LessThan, Repository } from 'typeorm';

import { NotifyService } from '../../notification/services';
import { VoterService } from '../../voter/voter.service';
import { Checkin, CheckinRequest, CheckinRequestGuardian } from '../entities';
import { CheckinRequestTypeEnum } from '../enums';
import { ICheckinRequest } from '../interfaces';
import checkinNotifications from '../notifications/checkin-notifications';

@Injectable()
export class CheckinCronService {
  constructor(
    @InjectRepository(Checkin)
    private readonly checkinRepository: Repository<Checkin>,
    @InjectRepository(CheckinRequest)
    private readonly checkinRequestRepository: Repository<CheckinRequest>,
    @InjectRepository(CheckinRequestGuardian)
    private readonly checkinRequestGuardianRepository: Repository<CheckinRequestGuardian>,
    private readonly voterService: VoterService,
    private readonly notifyService: NotifyService,
  ) {}

  // Create planned checkins every 10 seconds
  @Cron(CronExpression.EVERY_10_SECONDS)
  async createCheckins(): Promise<void> {
    const requests = await this.checkinRequestRepository.find({
      relations: ['targetAccount', 'requester'],
      where: {
        nextCheckinAt: LessThan(new Date()),
      },
    });

    for (const request of requests) {
      await this.createCheckin({ request });
    }
  }

  async createCheckin({ request }: { request: ICheckinRequest }): Promise<void> {
    // One time
    if (request.type === CheckinRequestTypeEnum.ONE_TIME) {
      request.nextCheckinAt = null;
    }

    // Periodical
    if (request.type === CheckinRequestTypeEnum.PERIODICAL) {
      const nextCheckinAt = addMinutes(new Date(), request.periodInMinutes);

      if (nextCheckinAt > request.endsAt) {
        request.nextCheckinAt = null;
      } else {
        request.nextCheckinAt = nextCheckinAt;
      }
    }

    await this.checkinRequestRepository.save(request);
    const checkin = await this.checkinRepository.save(new Checkin({ request }));

    const notification = checkinNotifications.createCheckin({ request, checkin });

    this.notifyService.notifyAccount({ account: request.targetAccount, notification });
  }
}
