import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { subSeconds } from 'date-fns';
import { LessThan, Repository } from 'typeorm';

import { configService } from '../../../config/config.service';
import { GuardianService } from '../../guardian/services';
import { VoterService } from '../../voter/voter.service';
import { Sos } from '../entities';
import { SosStateEnum } from '../enums';
import { ISos } from '../interfaces';
import sosNotifications from '../notifications/sos-notifications';

@Injectable()
export class SosEmergencyService {
  constructor(
    @InjectRepository(Sos)
    private readonly sosRepository: Repository<Sos>,
    private readonly voterService: VoterService,
    private readonly guardianService: GuardianService,
  ) {}

  // Alarm guardians after certain time when the SOS is not in PIN_ENTERED state
  @Cron(CronExpression.EVERY_SECOND)
  async findEmergency(): Promise<void> {
    const { sosEmergencyDelay } = configService.getAppConfig();

    const sosEmergencyDelayDate = subSeconds(new Date(), sosEmergencyDelay);

    const soses = await this.sosRepository.find({
      relations: ['account'],
      where: {
        state: SosStateEnum.RELEASED,
        buttonReleasedAt: LessThan(sosEmergencyDelayDate),
      },
    });

    soses.forEach((sos) => this.processEmergency({ sos }));
  }

  async processEmergency({ sos }: { sos: ISos }): Promise<void> {
    sos.state = SosStateEnum.EMERGENCY;
    sos.guardiansContactedAt = new Date();

    await this.sosRepository.save(sos);

    const notification = sosNotifications.createSosEmergency({ sos });

    this.guardianService.notifyGuardians({ account: sos.account, notification });
  }
}
