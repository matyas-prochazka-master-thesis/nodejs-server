import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { IAccount } from '../../account/interfaces';
import { GqlAuthJwtGuard } from '../../auth/guards';
import { CheckinRequestDto, CheckinRequestPeriodicalDto, CheckinDto } from '../dto';
import { Checkin, CheckinRequest } from '../entities';
import { CheckinService } from '../services';

@Resolver(() => Checkin)
@UseFilters(GqlFilter)
@UseGuards(GqlAuthJwtGuard)
export class CheckinResolver {
  constructor(private readonly checkinService: CheckinService) {}

  @Mutation(() => Boolean)
  async requestCheckin(@GqlAccount() account: IAccount, @Args('checkinRequest') checkinRequestDto: CheckinRequestDto) {
    await this.checkinService.requestCheckin({ account, checkinRequestDto });

    return true;
  }

  @Mutation(() => Boolean)
  async requestPeriodicalCheckin(
    @GqlAccount() account: IAccount,
    @Args('periodicalCheckinRequest') checkinRequestPeriodicalDto: CheckinRequestPeriodicalDto,
  ) {
    await this.checkinService.requestPeriodicalCheckin({ account, checkinRequestPeriodicalDto });

    return true;
  }

  @Mutation(() => Boolean)
  async cancelPeriodicalRequest(
    @GqlAccount() account: IAccount,
    @Args('checkinRequestId', { type: () => ID }) checkinRequestId: number,
  ) {
    await this.checkinService.cancelPeriodicalRequest({ account, checkinRequestId });

    return true;
  }

  @Mutation(() => Boolean)
  async checkin(@GqlAccount() account: IAccount, @Args('checkin') checkinDto: CheckinDto) {
    await this.checkinService.checkin({ account, checkinDto });

    return true;
  }

  @Query(() => [CheckinRequest])
  async getRequestsForMe(@GqlAccount() account: IAccount) {
    return this.checkinService.getRequestsForAccount({ account });
  }

  @Query(() => [CheckinRequest])
  async getRequestsIFollow(@GqlAccount() account: IAccount) {
    return this.checkinService.getRequestsAccountFollows({ account });
  }

  @Query(() => CheckinRequest)
  async getRequest(
    @GqlAccount() account: IAccount,
    @Args('checkinRequestId', { type: () => ID }) checkinRequestId: number,
  ) {
    return this.checkinService.getCheckinRequest({ account, checkinRequestId });
  }

  @Query(() => [Checkin])
  async getCheckinsByRequest(
    @GqlAccount() account: IAccount,
    @Args('checkinRequestId', { type: () => ID }) checkinRequestId: number,
  ) {
    return this.checkinService.getCheckinsByRequest({ account, checkinRequestId });
  }

  @Query(() => [Checkin])
  async getUncheckedCheckinsForMe(@GqlAccount() account: IAccount) {
    return this.checkinService.getUncheckedCheckinsForMe({ account });
  }
}
