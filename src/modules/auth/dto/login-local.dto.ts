import { Field, InputType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail } from 'class-validator';

@InputType()
export class LoginLocalDto {
  @Field()
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @Field()
  @IsNotEmpty()
  @ApiProperty()
  readonly password: string;
}
