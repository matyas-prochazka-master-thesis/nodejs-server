import { Field, InputType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty } from 'class-validator';

import { ForgottenPasswordSourcesEnum } from '../enums';

@InputType()
export class ForgottenPasswordDto {
  @Field()
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field(() => ForgottenPasswordSourcesEnum, { nullable: true })
  @ApiProperty()
  //  @IsNotEmpty()
  //  @IsEnum(ForgottenPasswordSourcesEnum)
  source: ForgottenPasswordSourcesEnum;
}
