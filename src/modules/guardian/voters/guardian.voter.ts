import { IAccount } from '../../account/interfaces';
import { GuardianAttributes } from '../../voter/attributes';
import { IVoter } from '../../voter/interfaces';
import { IGuardian } from '../interfaces';

export class GuardianVoter extends IVoter {
  async supports(attribute: string): Promise<boolean> {
    return (<any>Object).values(GuardianAttributes).includes(attribute);
  }

  async voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    switch (attribute) {
      case GuardianAttributes.REMOVE_GUARDIAN:
        return this.removeGuardian(account, subject);
      case GuardianAttributes.ACCEPT_GUARDIAN:
        return this.acceptGuardian(account, subject);
    }

    return false;
  }

  async removeGuardian(account: IAccount, guardian: IGuardian): Promise<boolean> {
    return account.id === guardian.account.id;
  }

  async acceptGuardian(account: IAccount, guardian: IGuardian): Promise<boolean> {
    return account.id === guardian.guardian.id;
  }
}
