import { ForbiddenException, NotFoundException } from '../../../shared/exceptions';

const createTripNotFound = () => {
  return new NotFoundException({ message: 'Trip not found' });
};

const createCannotUpdateTrip = () => {
  return new ForbiddenException({ message: 'Cannot update this trip' });
};

const createCannotViewTrip = () => {
  return new ForbiddenException({ message: 'Cannot view this trip' });
};

const createTripDestinationNotFound = () => {
  return new NotFoundException({ message: 'Trip destination not found' });
};

const createCannotViewTripDestination = () => {
  return new ForbiddenException({ message: 'Cannot view this trip destination' });
};

export default {
  createTripNotFound,
  createCannotUpdateTrip,
  createCannotViewTrip,
  createTripDestinationNotFound,
  createCannotViewTripDestination,
};
