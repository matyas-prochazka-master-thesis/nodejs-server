import {MigrationInterface, QueryRunner} from "typeorm";

export class tripQuickActions1649497530436 implements MigrationInterface {
    name = 'tripQuickActions1649497530436'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip" ADD "quickActions" json NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip" DROP COLUMN "quickActions"`);
    }

}
