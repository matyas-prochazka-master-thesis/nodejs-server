import { JwtTypeEnum } from '../enums';

export class TokenDto {
  readonly email: string;

  readonly sub: number;

  readonly role: string;

  readonly type: JwtTypeEnum;
}
