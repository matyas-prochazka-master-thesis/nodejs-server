import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class SosButtonReleasedDto {
  @Field()
  @IsNotEmpty()
  readonly idFromClient: string;
}
