import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { GqlAuthJwtGuard } from '../../auth/guards';
import { AccountUpdateDto, ProfilePictureUpdateDto, UsernameUpdateDto } from '../dto';
import { Account } from '../entities';
import { IAccount } from '../interfaces';
import { AccountService } from '../services';

@Resolver(() => Account)
@UseFilters(GqlFilter)
@UseGuards(GqlAuthJwtGuard)
export class AccountResolver {
  constructor(private readonly accountService: AccountService) {}

  @Query(() => Account)
  async me(@GqlAccount() account: IAccount) {
    return this.accountService.find({ accountId: account.id });
  }

  @Mutation(() => Account)
  async updateUsername(@GqlAccount() account: IAccount, @Args('usernameUpdate') usernameUpdateDto: UsernameUpdateDto) {
    return this.accountService.updateUsername({ account, usernameUpdateDto });
  }

  @Mutation(() => Account)
  async updateAccount(@GqlAccount() account: IAccount, @Args('accountUpdate') accountUpdateDto: AccountUpdateDto) {
    return this.accountService.updateAccount({ account, accountUpdateDto });
  }

  @Mutation(() => Account)
  async updateProfilePicture(
    @GqlAccount() account: IAccount,
    @Args('profilePictureUpdate') profilePictureUpdateDto: ProfilePictureUpdateDto,
  ) {
    return this.accountService.updateProfilePicture({ account, profilePictureUpdateDto });
  }
}
