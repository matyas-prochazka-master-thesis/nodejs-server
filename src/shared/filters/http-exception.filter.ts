import { ExceptionFilter, Catch, ArgumentsHost, HttpStatus, HttpException } from '@nestjs/common';
import { Response } from 'express';

import { BaseException } from '../exceptions';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    let status;
    let name;
    let errors;

    if (exception instanceof BaseException) {
      status = exception.getStatus();
      name = exception.getName();
      errors = exception.getPayload();
    } else if (exception instanceof HttpException) {
      status = exception.getStatus();
      name = exception.message;
    } else {
      console.log(exception);

      status = HttpStatus.INTERNAL_SERVER_ERROR;
      name = 'Internal server error';
    }

    response.status(status).json({
      status,
      name,
      errors,
      timestamp: new Date().toISOString(),
    });
  }
}
