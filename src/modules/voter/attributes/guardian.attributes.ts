export enum GuardianAttributes {
  REMOVE_GUARDIAN = 'guardian_remove',
  ACCEPT_GUARDIAN = 'guardian_accept',
}
