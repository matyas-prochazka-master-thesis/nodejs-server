import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';

import { configService } from '../../config/config.service';
import { AccountModule } from '../account/account.module';
import { Account } from '../account/entities';
import { FirebaseModule } from '../firebase/firebaseModule';
import { MailModule } from '../mail/mail.module';
import { PasswordReset } from './entities';
import { AuthResolver } from './resolvers';
import { FacebookAuthService, GoogleAuthService, AuthService, AppleAuthService } from './services';
import { LocalStrategy, JwtStrategy } from './strategies';
import { JwtRefreshStrategy } from './strategies/jwt-refresh.strategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([Account, PasswordReset]),
    AccountModule,
    PassportModule,
    JwtModule.register(configService.getJwt()),
    MailModule,
    FirebaseModule,
  ],
  providers: [
    AppleAuthService,
    AuthService,
    LocalStrategy,
    JwtStrategy,
    JwtRefreshStrategy,
    AuthResolver,
    FacebookAuthService,
    GoogleAuthService,
  ],
  controllers: [],
})
export class AuthModule {}
