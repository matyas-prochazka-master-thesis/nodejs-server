export enum CheckinAttributes {
  REQUEST_CHECKIN = 'checkin_request',
  CHECKIN_CHECK = 'checkin_check',
  CHECKIN_REQUEST_VIEW = 'checkin_request_view',
  CHECKIN_REQUEST_UPDATE = 'checkin_request_update',
}
