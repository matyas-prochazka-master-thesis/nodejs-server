import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { File } from '../../storage/entities';
import { IFile } from '../../storage/interfaces';
import { TripActivityKeyEnum } from '../enums/trip-activity-key.enum';
import { ITrip, ITripActivity } from '../interfaces';
import { Trip } from './trip.entity';

@ObjectType()
@Entity()
export class TripActivity implements ITripActivity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Trip)
  @ManyToOne(() => Trip)
  trip: ITrip;

  @Field(() => TripActivityKeyEnum)
  @Column()
  key: TripActivityKeyEnum;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  latitude: number | null;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  longitude: number | null;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  batteryLevel: number | null;

  @Field()
  @Column('json')
  payload: string;

  @Field(() => File, { nullable: true })
  @OneToOne(() => File, {
    nullable: true,
  })
  @JoinColumn()
  attachment: IFile | null;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  constructor(props: {
    trip: ITrip;
    key: TripActivityKeyEnum;
    latitude: number | null;
    longitude: number | null;
    batteryLevel: number | null;
    payload: string;
    attachment: IFile | null;
  }) {
    this.trip = props?.trip;
    this.key = props?.key;
    this.latitude = props?.latitude;
    this.longitude = props?.longitude;
    this.batteryLevel = props?.batteryLevel;
    this.payload = props?.payload;
    this.attachment = props?.attachment;
  }
}
