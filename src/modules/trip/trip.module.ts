import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GuardianModule } from '../guardian/guardian.module';
import { NotificationModule } from '../notification/notification.module';
import { StorageModule } from '../storage/storage.module';
import { Trip, TripActivity, TripDestination } from './entities';
import { TripResolver } from './resolvers';
import { TripActivityService, TripService, TripDestinationService } from './services';
import { TripVoter } from './voters/trip.voter';

@Module({
  imports: [TypeOrmModule.forFeature([Trip, TripActivity, TripDestination]), GuardianModule, StorageModule],
  providers: [TripService, TripActivityService, TripDestinationService, TripResolver, TripVoter],
  controllers: [],
  exports: [],
})
export class TripModule {}
