import {MigrationInterface, QueryRunner} from "typeorm";

export class tripActivityTypeFix1649507096699 implements MigrationInterface {
    name = 'tripActivityTypeFix1649507096699'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP COLUMN "latitude"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD "latitude" numeric`);
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD "longitude" numeric`);
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP COLUMN "batteryLevel"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD "batteryLevel" numeric`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP COLUMN "batteryLevel"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD "batteryLevel" integer`);
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD "longitude" integer`);
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP COLUMN "latitude"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD "latitude" integer`);
    }

}
