import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { IGuardian } from '../interfaces';

@ObjectType()
@Entity()
export class Guardian implements IGuardian {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Account)
  @ManyToOne(() => Account)
  account: IAccount;

  @Field(() => Account)
  @ManyToOne(() => Account)
  guardian: IAccount;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  acceptedAt: Date | null;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  constructor(props: { account: IAccount; guardian: IAccount }) {
    this.account = props?.account;
    this.guardian = props?.guardian;
  }
}
