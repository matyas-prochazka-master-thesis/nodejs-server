import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class AccountUpdateDto {
  @Field()
  @IsNotEmpty()
  readonly username: string;

  @Field()
  @IsNotEmpty()
  readonly name: string;

  @Field({ nullable: true })
  readonly telephone: string | null;
}
