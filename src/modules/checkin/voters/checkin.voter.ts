import { Inject } from '@nestjs/common';

import { IAccount } from '../../account/interfaces';
import { GuardianService } from '../../guardian/services';
import { CheckinAttributes } from '../../voter/attributes';
import { IVoter } from '../../voter/interfaces';
import { ICheckin, ICheckinRequest } from '../interfaces';

export class CheckinVoter extends IVoter {
  @Inject(GuardianService)
  private readonly guardianService: GuardianService;

  async supports(attribute: string): Promise<boolean> {
    return (<any>Object).values(CheckinAttributes).includes(attribute);
  }

  async voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    switch (attribute) {
      case CheckinAttributes.REQUEST_CHECKIN:
        return this.canRequest(account, subject);
      case CheckinAttributes.CHECKIN_CHECK:
        return this.canCheck(account, subject);
      case CheckinAttributes.CHECKIN_REQUEST_VIEW:
        return this.canViewRequest(account, subject);
      case CheckinAttributes.CHECKIN_REQUEST_UPDATE:
        return this.canUpdateRequest(account, subject);
    }

    return false;
  }

  // Can account request checkin from target account
  async canRequest(account: IAccount, targetAccount: IAccount): Promise<boolean> {
    if (account.id === targetAccount.id) {
      return true;
    }

    const guardians = await this.guardianService.getGuardiansByAccount({ account });

    return guardians.some((guardian) => guardian.guardian.id === targetAccount.id);
  }

  // Can account check checkin
  async canCheck(account: IAccount, checkin: ICheckin): Promise<boolean> {
    return checkin.request.targetAccount.id === account.id;
  }

  // Can account view checkin request
  async canViewRequest(account: IAccount, checkinRequest: ICheckinRequest): Promise<boolean> {
    if (account.id === checkinRequest.targetAccount.id) {
      return true;
    }

    if (account.id === checkinRequest.requester.id) {
      return true;
    }

    return checkinRequest.guardians.some((guardian) => guardian.account.id === account.id);
  }

  // Can account update checkin request
  async canUpdateRequest(account: IAccount, checkinRequest: ICheckinRequest): Promise<boolean> {
    if (account.id === checkinRequest.targetAccount.id) {
      return true;
    }

    if (account.id === checkinRequest.requester.id) {
      return true;
    }

    return false;
  }
}
