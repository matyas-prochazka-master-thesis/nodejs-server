import { BadRequestException, ForbiddenException } from '../../../shared/exceptions';

const createAppleInfoNotProvided = () => {
  return new BadRequestException({ message: 'Apple info not provided' });
};

const createInvalidGoogleAuthToken = () => {
  return new ForbiddenException({ message: 'invalid google auth token' });
};

const createInvalidOldPassword = () => {
  return new ForbiddenException({ message: 'Invalid old password' });
};

export default {
  createAppleInfoNotProvided,
  createInvalidGoogleAuthToken,
  createInvalidOldPassword,
};
