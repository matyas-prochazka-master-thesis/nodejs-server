import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import AppleAuth from 'apple-auth';
import { Repository } from 'typeorm';

import { configService } from '../../../config/config.service';
import { Account } from '../../account/entities';
import { AccountSourceEnum } from '../../account/enums';
import { IAccount } from '../../account/interfaces';
import { AppleAuthDto } from '../dto';
import authExceptions from '../exceptions/auth-exceptions';
import { AuthService } from './auth.service';

@Injectable()
export class AppleAuthService {
  private appleAuth;

  constructor(
    @InjectRepository(Account)
    private accountRepository: Repository<Account>,
    private authService: AuthService,
    private jwtService: JwtService,
  ) {
    const config = configService.getAppleConfig();

    this.appleAuth = new AppleAuth(config, config.key, 'text');
  }

  async auth(appleAuthDto: AppleAuthDto) {
    const { code, source } = appleAuthDto;

    const response = await this.appleAuth.accessToken(code);

    const idToken = await this.jwtService.decode(response.id_token);

    if (typeof idToken === 'string' || !idToken.email) {
      throw authExceptions.createAppleInfoNotProvided();
    }

    return this.authService.createToken(
      await this.getOrCreate({ appleId: idToken.sub, email: idToken.email.toLowerCase(), source }),
    );
  }

  private async getOrCreate({
    appleId,
    email,
    source,
  }: {
    appleId: string;
    email: string;
    source: AccountSourceEnum | null;
  }): Promise<IAccount> {
    let account = await this.accountRepository.findOne({ appleId });

    if (account) {
      return account;
    }

    account = await this.accountRepository.findOne({ email });

    if (!account) {
      account = new Account({ email, source });
    }

    account.appleId = appleId;

    return this.accountRepository.save(account);
  }
}
