export interface ITripQuickAction {
  id: string;
  name: string;
  icon: string;
}
