import { Field, ID, ObjectType } from '@nestjs/graphql';
import * as argon2 from 'argon2';
import { Exclude } from 'class-transformer';
import { Entity, Column, OneToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';

import { File } from '../../storage/entities';
import { IFile } from '../../storage/interfaces';
import { AccountRolesEnum, AccountSourceEnum } from '../enums';
import { IAccount } from '../interfaces';

@ObjectType()
@Entity()
export class Account implements IAccount {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field({ nullable: true })
  @Column({ unique: true, nullable: true })
  username: string | null;

  @Field()
  @Column()
  email: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  name: string | null;

  @Field({ nullable: true })
  @Column({ nullable: true })
  telephone: string | null;

  @Column({ nullable: true })
  @Exclude({ toPlainOnly: true })
  password: string | null;

  @Field(() => AccountRolesEnum)
  @Column({ default: AccountRolesEnum.ROLE_REGISTERED })
  role: AccountRolesEnum;

  @Field(() => File, { nullable: true })
  @OneToOne(() => File, {
    nullable: true,
  })
  @JoinColumn()
  profilePicture: IFile | null;

  @Field(() => AccountSourceEnum, { nullable: true })
  @Column({ nullable: true })
  source: AccountSourceEnum | null;

  @Column({ nullable: true, unique: true })
  appleId: string | null;

  @Column({ nullable: true, unique: true })
  facebookId: string | null;

  @Column({ nullable: true, unique: true })
  googleId: string | null;

  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  registeredAt: Date;

  constructor(props: { email: string; source: AccountSourceEnum | null }) {
    this.email = props?.email;
    this.source = props?.source;

    this.role = AccountRolesEnum.ROLE_REGISTERED;
  }

  async setPassword(password: string): Promise<Account> {
    this.password = await argon2.hash(password);

    return this;
  }
}
