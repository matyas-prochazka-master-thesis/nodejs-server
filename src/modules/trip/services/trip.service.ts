import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { GuardianService } from '../../guardian/services';
import { TripAttributes } from '../../voter/attributes';
import { VoterService } from '../../voter/voter.service';
import { TripCreateDto } from '../dto';
import { Trip } from '../entities';
import { TripStateEnum } from '../enums';
import tripExceptions from '../exceptions/trip-exceptions';
import { ITrip, ITripDestination } from '../interfaces';
import tripNotifications from '../notifications/trip-notifications';
import { TripDestinationService } from './trip-destination.service';

@Injectable()
export class TripService {
  constructor(
    @InjectRepository(Trip)
    private readonly tripRepository: Repository<Trip>,
    private readonly tripDestinationService: TripDestinationService,
    private readonly guardianService: GuardianService,
    private readonly voterService: VoterService,
  ) {}

  async findTrip({ tripId }: { tripId: number }): Promise<ITrip> {
    const trip = await this.tripRepository.findOne(tripId, {
      relations: ['account', 'account.profilePicture', 'destination'],
    });

    if (!trip) {
      throw tripExceptions.createTripNotFound();
    }

    return trip;
  }

  async getActiveTrip({ account }: { account: IAccount }): Promise<ITrip | null> {
    return this.tripRepository.findOne({
      relations: ['destination'],
      where: {
        account,
        state: TripStateEnum.STARTED,
      },
    });
  }

  async getTrip({ account, tripId }: { account: IAccount; tripId: number }): Promise<ITrip> {
    const trip = await this.findTrip({ tripId });

    if (!(await this.voterService.isGranted(TripAttributes.VIEW, trip, account))) {
      throw tripExceptions.createCannotViewTrip();
    }

    return trip;
  }

  async getTripsByAccount({ account }: { account: IAccount }): Promise<ITrip[]> {
    return this.tripRepository.find({
      relations: ['destination'],
      where: {
        account,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async getAccountGuardiansTrips({ account }: { account: IAccount }): Promise<ITrip[]> {
    const guardians = await this.guardianService.getGuardiansByAccount({ account });

    return this.tripRepository.find({
      relations: ['destination', 'account', 'account.profilePicture'],
      where: {
        account: In(guardians.map((guardian) => guardian.guardian.id)),
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async createTrip({ account, tripCreateDto }: { account: IAccount; tripCreateDto: TripCreateDto }): Promise<ITrip> {
    const { shouldArriveAt, tripDestinationCreateDto, tripDestinationId, quickActions } = tripCreateDto;

    let destination: ITripDestination | null = null;

    if (tripDestinationId) {
      destination = await this.tripDestinationService.getDestination({ account, tripDestinationId });
    } else if (tripDestinationCreateDto) {
      destination = await this.tripDestinationService.createDestination({
        account,
        tripDestinationCreateDto,
      });
    }

    const trip = await this.tripRepository.save(new Trip({ account, shouldArriveAt, destination, quickActions }));

    const notification = tripNotifications.createCreateTrip({ trip });

    this.guardianService.notifyGuardians({ account, notification });

    return trip;
  }

  async endTrip({ account, tripId }: { account: IAccount; tripId: number }): Promise<void> {
    const trip = await this.findTrip({ tripId });

    if (!(await this.voterService.isGranted(TripAttributes.UPDATE, trip, account))) {
      throw tripExceptions.createCannotUpdateTrip();
    }

    trip.state = TripStateEnum.FINISHED;

    await this.tripRepository.save(trip);

    const notification = tripNotifications.createTripEnded({ trip });

    this.guardianService.notifyGuardians({ account, notification });
  }
}
