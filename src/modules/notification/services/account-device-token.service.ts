import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { AccountDeviceToken } from '../entities';

@Injectable()
export class AccountDeviceTokenService {
  constructor(
    @InjectRepository(AccountDeviceToken)
    private accountDeviceTokenRepository: Repository<AccountDeviceToken>,
  ) {}

  async assignDeviceToken({ account, deviceToken }: { account: IAccount; deviceToken: string }): Promise<void> {
    // First try to find if the token is already assigned
    let accountDeviceToken = await this.accountDeviceTokenRepository.findOne({
      relations: ['account'],
      where: {
        deviceToken,
      },
    });

    if (!accountDeviceToken) {
      accountDeviceToken = new AccountDeviceToken({ account, deviceToken });
    } else {
      // Update the account
      // For example the user logged out in the app and signed in with different account
      accountDeviceToken.updatedAt = new Date();

      if (account.id !== accountDeviceToken.account.id) {
        accountDeviceToken.account = account;
      }
    }

    await this.accountDeviceTokenRepository.save(accountDeviceToken);
  }
}
