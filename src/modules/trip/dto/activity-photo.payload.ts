import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class ActivityPhotoPayload {
  @Field()
  @IsNotEmpty()
  readonly photo: string;
}
