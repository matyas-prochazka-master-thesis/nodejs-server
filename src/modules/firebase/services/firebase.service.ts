import { Injectable } from '@nestjs/common';
import * as Admin from 'firebase-admin';

import { configService } from '../../../config/config.service';
import { FirebaseFake } from './firebase.fake';

@Injectable()
export class FirebaseService {
  private readonly app;

  private readonly apiKey;

  // Init fake service if not in production
  constructor() {
    if (configService.isProduction()) {
      this.app = Admin.initializeApp({
        credential: Admin.credential.cert(configService.getFirebaseConfig()),
      });
    } else {
      // this.app = Admin.initializeApp({
      //  credential: Admin.credential.cert(configService.getFirebaseConfig()),
      // });
      this.app = new FirebaseFake();
    }

    this.apiKey = configService.getFirebaseKey();
  }

  async sendNotificationForDevices(content: any, data: any, devices: string[]): Promise<void> {
    const message = {
      notification: content,
      data,
      tokens: devices,
      android: {
        collapseKey: 'fooder',
      },
      apns: {
        payload: {
          aps: {
            'mutable-content': 1,
          },
        },
      },
    };

    await this.app.messaging().sendMulticast(message);
  }

  async sendNotificationForTopic(content: any, data: any, topic: string): Promise<void> {
    const message = {
      notification: content,
      data,
      topic,
      android: {
        collapseKey: 'fooder',
      },
      apns: {
        payload: {
          aps: {
            'mutable-content': 1,
          },
        },
      },
    };

    await this.app.messaging().send(message);
  }

  async sendNotificationForCondition(content: any, data: any, condition: string): Promise<void> {
    const message = {
      notification: content,
      data,
      condition,
      android: {
        collapseKey: 'fooder',
      },
      apns: {
        payload: {
          aps: {
            'mutable-content': 1,
          },
        },
      },
    };

    this.app.messaging().send(message);
  }

  async subscribeToTopic(tokens: string[], topic: string): Promise<void> {
    this.app.messaging().subscribeToTopic(tokens, topic);
  }

  async unsubscribeFromTopic(tokens: string[], topic: string): Promise<void> {
    this.app.messaging().unsubscribeFromTopic(tokens, topic);
  }
}
