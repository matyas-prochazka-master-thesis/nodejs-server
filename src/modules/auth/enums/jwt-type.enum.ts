export enum JwtTypeEnum {
  ACCESS = 'access',
  REFRESH = 'refresh',
}
