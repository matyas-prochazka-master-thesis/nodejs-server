
export interface IFile {
  id: number;

  path: string;
  mimeType: string;
  size: number;

  addedAt: Date;
}