import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { IAccount } from '../../account/interfaces';
import { GqlAuthJwtGuard } from '../../auth/guards';
import { AccountDeviceToken } from '../entities';
import { AccountDeviceTokenService } from '../services';

@Resolver(() => AccountDeviceToken)
@UseGuards(GqlAuthJwtGuard)
@UseFilters(GqlFilter)
export class AccountDeviceTokenResolver {
  constructor(private accountDeviceTokenService: AccountDeviceTokenService) {}

  @Mutation(() => Boolean)
  async assignDeviceToken(@GqlAccount() account: IAccount, @Args('deviceToken') deviceToken: string) {
    await this.accountDeviceTokenService.assignDeviceToken({ account, deviceToken });

    return true;
  }
}
