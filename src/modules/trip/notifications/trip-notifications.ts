import { ITrip } from '../interfaces';

const createCreateTrip = ({ trip }: { trip: ITrip }) => {
  return {
    title: `${trip.account.username ?? trip.account.email} just started a trip!`,
    body: `Your friend has just started a trip`,
    payload: {
      type: 'warning',
      open: 'screens-my-guardians-trip-history',
      'localized-title-en': `${trip.account.username ?? trip.account.email} just started a trip!`,
      'localized-body-en': `Your friend has just started a trip`,
    },
  };
};

const createTripEnded = ({ trip }: { trip: ITrip }) => {
  return {
    title: `${trip.account.username ?? trip.account.email} just ended a trip!`,
    body: `Your friend has just ended a trip`,
    payload: {
      type: 'warning',
      open: 'screens-my-guardians-trip-history',
      'localized-title-en': `${trip.account.username ?? trip.account.email} just ended a trip!`,
      'localized-body-en': `Your friend has just ended a trip`,
    },
  };
};

const createAddedMessageToTrip = ({ trip, message }: { trip: ITrip; message: string }) => {
  return {
    title: `${trip.account.username ?? trip.account.email} added a message to a trip!`,
    body: `The message: ${message}`,
    payload: {
      type: 'warning',
      open: 'screens-my-guardians-trip-history',
      'localized-title-en': `${trip.account.username ?? trip.account.email} added a message to a trip!`,
      'localized-body-en': `The message: ${message}`,
    },
  };
};

const createAddedPhotoToTrip = ({ trip }: { trip: ITrip }) => {
  return {
    title: `${trip.account.username ?? trip.account.email} added a photo to a trip!`,
    body: `Open the guardians trip history to view the photo`,
    payload: {
      type: 'warning',
      open: 'screens-my-guardians-trip-history',
      'localized-title-en': `${trip.account.username ?? trip.account.email} added a photo to a trip!`,
      'localized-body-en': `Open the guardians trip history to view the photo`,
    },
  };
};

const createAddedQuickActionToTrip = ({ trip, quickActionName }: { trip: ITrip; quickActionName: string }) => {
  return {
    title: `${trip.account.username ?? trip.account.email} added a quick action to a trip!`,
    body: `The quick action name: ${quickActionName}`,
    payload: {
      type: 'warning',
      open: 'screens-my-guardians-trip-history',
      'localized-title-en': `${trip.account.username ?? trip.account.email} added a quick action to a trip!`,
      'localized-body-en': `The quick action name: ${quickActionName}`,
    },
  };
};

export default {
  createCreateTrip,
  createTripEnded,
  createAddedMessageToTrip,
  createAddedPhotoToTrip,
  createAddedQuickActionToTrip,
};
