import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { StorageModule } from '../storage/storage.module';
import { Account } from './entities';
import { AccountResolver } from './resolvers';
import { AccountService } from './services';
import { RoleVoter } from './voters';

@Module({
  imports: [TypeOrmModule.forFeature([Account]), StorageModule],
  providers: [AccountResolver, AccountService, RoleVoter],
  controllers: [],
  exports: [AccountService],
})
export class AccountModule {}
