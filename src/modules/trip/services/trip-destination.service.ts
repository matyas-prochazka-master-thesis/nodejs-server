import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { IAccount } from '../../account/interfaces';
import { TripAttributes } from '../../voter/attributes';
import { VoterService } from '../../voter/voter.service';
import { TripDestinationCreateDto } from '../dto';
import { Trip, TripDestination } from '../entities';
import tripExceptions from '../exceptions/trip-exceptions';
import { ITripDestination } from '../interfaces';

@Injectable()
export class TripDestinationService {
  constructor(
    @InjectRepository(Trip)
    private readonly tripRepository: Repository<Trip>,
    @InjectRepository(TripDestination)
    private readonly tripDestinationRepository: Repository<TripDestination>,
    private readonly voterService: VoterService,
  ) {}

  async findDestination({ tripDestinationId }: { tripDestinationId: number }): Promise<ITripDestination> {
    const destination = await this.tripDestinationRepository.findOne(tripDestinationId, {
      relations: ['account'],
    });

    if (!destination) {
      throw tripExceptions.createTripDestinationNotFound();
    }

    return destination;
  }

  async getDestination({
    account,
    tripDestinationId,
  }: {
    account: IAccount;
    tripDestinationId: number;
  }): Promise<ITripDestination> {
    const destination = await this.findDestination({ tripDestinationId });

    if (!(await this.voterService.isGranted(TripAttributes.UPDATE, destination, account))) {
      throw tripExceptions.createCannotViewTripDestination();
    }

    return destination;
  }

  async createDestination({
    account,
    tripDestinationCreateDto,
  }: {
    account: IAccount;
    tripDestinationCreateDto: TripDestinationCreateDto;
  }): Promise<ITripDestination> {
    const { name, latitude, longitude } = tripDestinationCreateDto;

    return this.tripDestinationRepository.save(new TripDestination({ account, name, latitude, longitude }));
  }

  async getDestinationsByAccount({ account }: { account: IAccount }): Promise<ITripDestination[]> {
    return this.tripDestinationRepository.find({
      where: {
        account,
      },
    });
  }

  async toggleFavouriteDestination({ account, tripDestinationId }): Promise<void> {
    const destination = await this.findDestination({ tripDestinationId });

    if (!(await this.voterService.isGranted(TripAttributes.UPDATE, destination, account))) {
      throw tripExceptions.createCannotViewTripDestination();
    }

    destination.favourite = !destination.favourite;

    await this.tripDestinationRepository.save(destination);
  }
}
