import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AccountModule } from '../account/account.module';
import { NotificationModule } from '../notification/notification.module';
import { Guardian } from './entities';
import { GuardianResolver } from './resolvers';
import { GuardianService } from './services';
import { GuardianVoter } from './voters/guardian.voter';

@Module({
  imports: [TypeOrmModule.forFeature([Guardian]), AccountModule, NotificationModule],
  providers: [GuardianService, GuardianVoter, GuardianResolver],
  controllers: [],
  exports: [GuardianService],
})
export class GuardianModule {}
