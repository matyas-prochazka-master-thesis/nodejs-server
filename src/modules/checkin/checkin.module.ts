import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AccountModule } from '../account/account.module';
import { GuardianModule } from '../guardian/guardian.module';
import { NotificationModule } from '../notification/notification.module';
import { Checkin, CheckinRequest, CheckinRequestGuardian } from './entities';
import { CheckinResolver } from './resolvers';
import { CheckinCronService, CheckinService } from './services';
import { CheckinVoter } from './voters/checkin.voter';

@Module({
  imports: [
    TypeOrmModule.forFeature([Checkin, CheckinRequest, CheckinRequestGuardian]),
    AccountModule,
    GuardianModule,
    NotificationModule,
  ],
  providers: [CheckinService, CheckinCronService, CheckinResolver, CheckinVoter],
  controllers: [],
  exports: [],
})
export class CheckinModule {}
