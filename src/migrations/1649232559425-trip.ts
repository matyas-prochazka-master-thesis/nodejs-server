import {MigrationInterface, QueryRunner} from "typeorm";

export class trip1649232559425 implements MigrationInterface {
    name = 'trip1649232559425'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "trip" ("id" SERIAL NOT NULL, "state" character varying NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "accountId" integer, CONSTRAINT "PK_714c23d558208081dbccb9d9268" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "trip_activity" ("id" SERIAL NOT NULL, "key" character varying NOT NULL, "latitude" integer NOT NULL, "longitude" integer NOT NULL, "batteryLevel" integer NOT NULL, "payload" json NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "tripId" integer, "attachmentId" integer, CONSTRAINT "REL_b651308c4b7031d9d167fc264b" UNIQUE ("attachmentId"), CONSTRAINT "PK_1f0dcce48f5da2201f933a00761" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "trip" ADD CONSTRAINT "FK_0d9cd3c82004d4967e92ac9af76" FOREIGN KEY ("accountId") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD CONSTRAINT "FK_95252005947b008ebfdaec81598" FOREIGN KEY ("tripId") REFERENCES "trip"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "trip_activity" ADD CONSTRAINT "FK_b651308c4b7031d9d167fc264ba" FOREIGN KEY ("attachmentId") REFERENCES "file"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP CONSTRAINT "FK_b651308c4b7031d9d167fc264ba"`);
        await queryRunner.query(`ALTER TABLE "trip_activity" DROP CONSTRAINT "FK_95252005947b008ebfdaec81598"`);
        await queryRunner.query(`ALTER TABLE "trip" DROP CONSTRAINT "FK_0d9cd3c82004d4967e92ac9af76"`);
        await queryRunner.query(`DROP TABLE "trip_activity"`);
        await queryRunner.query(`DROP TABLE "trip"`);
    }

}
