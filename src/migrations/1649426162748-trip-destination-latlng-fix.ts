import {MigrationInterface, QueryRunner} from "typeorm";

export class tripDestinationLatlngFix1649426162748 implements MigrationInterface {
    name = 'tripDestinationLatlngFix1649426162748'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_destination" DROP COLUMN "latitude"`);
        await queryRunner.query(`ALTER TABLE "trip_destination" ADD "latitude" numeric NOT NULL`);
        await queryRunner.query(`ALTER TABLE "trip_destination" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "trip_destination" ADD "longitude" numeric NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip_destination" DROP COLUMN "longitude"`);
        await queryRunner.query(`ALTER TABLE "trip_destination" ADD "longitude" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "trip_destination" DROP COLUMN "latitude"`);
        await queryRunner.query(`ALTER TABLE "trip_destination" ADD "latitude" integer NOT NULL`);
    }

}
