import { IAccount } from '../../account/interfaces';
import { SosStateEnum } from '../enums';

export interface ISos {
  id: number;
  // idFromClient is used to reference the entity so that mobile app does not have to wait until the first request is satisfied
  idFromClient: string;

  account: IAccount;

  state: SosStateEnum;

  initialLatitude: number;
  initialLongitude: number;

  buttonPressedAt: Date | null;
  buttonReleasedAt: Date | null;
  pinEnteredAt: Date | null;
  guardiansContactedAt: Date | null;

  createdAt: Date;
}
