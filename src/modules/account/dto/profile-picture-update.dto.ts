import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class ProfilePictureUpdateDto {
  @Field()
  @IsNotEmpty()
  readonly profilePictureBase64: string;
}
