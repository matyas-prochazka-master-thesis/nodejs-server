import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { IAccount } from '../../account/interfaces';
import { GqlAuthJwtGuard } from '../../auth/guards';
import {
  ActivityCreateDto,
  ActivityMessagePayload,
  ActivityPhotoPayload,
  ActivityQuickActionPayload,
  TripCreateDto,
} from '../dto';
import { Trip, TripActivity, TripDestination } from '../entities';
import { TripActivityService, TripDestinationService, TripService } from '../services';

@Resolver(() => Trip)
@UseFilters(GqlFilter)
@UseGuards(GqlAuthJwtGuard)
export class TripResolver {
  constructor(
    private readonly tripService: TripService,
    private readonly tripActivityService: TripActivityService,
    private readonly tripDestinationService: TripDestinationService,
  ) {}

  @Mutation(() => Trip)
  async createTrip(@GqlAccount() account: IAccount, @Args('tripCreate') tripCreateDto: TripCreateDto) {
    return this.tripService.createTrip({ account, tripCreateDto });
  }

  @Mutation(() => Boolean)
  async endTrip(@GqlAccount() account: IAccount, @Args('tripId', { type: () => ID! }) tripId: number) {
    await this.tripService.endTrip({ account, tripId });

    return true;
  }

  @Mutation(() => TripActivity)
  async addMessageToTrip(
    @GqlAccount() account: IAccount,
    @Args('tripId', { type: () => ID! }) tripId: number,
    @Args('activityCreate') activityCreateDto: ActivityCreateDto,
    @Args('activityMessage') activityMessagePayload: ActivityMessagePayload,
  ) {
    return this.tripActivityService.addMessageToTrip({ account, tripId, activityCreateDto, activityMessagePayload });
  }

  @Mutation(() => TripActivity)
  async addPhotoToTrip(
    @GqlAccount() account: IAccount,
    @Args('tripId', { type: () => ID! }) tripId: number,
    @Args('activityCreate') activityCreateDto: ActivityCreateDto,
    @Args('activityPhoto') activityPhotoPayload: ActivityPhotoPayload,
  ) {
    return this.tripActivityService.addPhotoToTrip({ account, tripId, activityCreateDto, activityPhotoPayload });
  }

  @Mutation(() => TripActivity)
  async addQuickActionToTrip(
    @GqlAccount() account: IAccount,
    @Args('tripId', { type: () => ID! }) tripId: number,
    @Args('activityCreate') activityCreateDto: ActivityCreateDto,
    @Args('activityQuickAction') activityQuickActionPayload: ActivityQuickActionPayload,
  ) {
    return this.tripActivityService.addQuickActionToTrip({
      account,
      tripId,
      activityCreateDto,
      activityQuickActionPayload,
    });
  }

  @Query(() => Trip, { nullable: true })
  async getActiveTrip(@GqlAccount() account: IAccount) {
    return this.tripService.getActiveTrip({ account });
  }

  @Query(() => [Trip])
  async getMyTrips(@GqlAccount() account: IAccount) {
    return this.tripService.getTripsByAccount({ account });
  }

  @Query(() => Trip)
  async getTrip(@GqlAccount() account: IAccount, @Args('tripId', { type: () => ID! }) tripId: number) {
    return this.tripService.getTrip({ account, tripId });
  }

  @Query(() => [Trip])
  async getMyGuardiansTrips(@GqlAccount() account: IAccount) {
    return this.tripService.getAccountGuardiansTrips({ account });
  }

  @Query(() => [TripActivity])
  async getActivitiesByTrip(@GqlAccount() account: IAccount, @Args('tripId', { type: () => ID! }) tripId: number) {
    return this.tripActivityService.getActivitiesByTrip({ account, tripId });
  }

  @Query(() => [TripDestination])
  async getMyTripDestinations(@GqlAccount() account: IAccount) {
    return this.tripDestinationService.getDestinationsByAccount({ account });
  }

  @Mutation(() => Boolean)
  async toggleFavouriteTripDestination(
    @GqlAccount() account: IAccount,
    @Args('tripDestinationId', { type: () => ID! }) tripDestinationId: number,
  ) {
    await this.tripDestinationService.toggleFavouriteDestination({ account, tripDestinationId });

    return true;
  }
}
