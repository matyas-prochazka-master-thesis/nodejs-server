import { Global, Module } from '@nestjs/common';

import { EmitterService } from './services';

@Global()
@Module({
  providers: [EmitterService],
  exports: [EmitterService],
})
export class EmitterModule {}
