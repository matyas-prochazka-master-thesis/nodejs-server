import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

import { Account } from '../../modules/account/entities';

export const GqlAccount = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): Account => {
    return GqlExecutionContext.create(ctx).getContext().req.user;
  },
);
