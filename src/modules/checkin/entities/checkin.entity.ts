import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { CheckinStateEnum } from '../enums';
import { ICheckin, ICheckinRequest } from '../interfaces';
import { CheckinRequest } from './checkin-request.entity';

@ObjectType()
@Entity()
export class Checkin implements ICheckin {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => CheckinRequest)
  @ManyToOne(() => CheckinRequest)
  request: ICheckinRequest;

  @Field(() => CheckinStateEnum)
  @Column()
  state: CheckinStateEnum;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  latitude: number | null;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  longitude: number | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  checkedAt: Date | null;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  constructor(props: { request: ICheckinRequest }) {
    this.request = props?.request;

    this.state = CheckinStateEnum.REQUESTED;
  }
}
