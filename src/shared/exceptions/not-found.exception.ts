import { HttpStatus } from '@nestjs/common';

import { BaseException, BaseExceptionError } from './base.exception';

export class NotFoundException extends BaseException {
  constructor(errors: BaseExceptionError | BaseExceptionError[]) {
    super('Not found', HttpStatus.NOT_FOUND, errors);
  }
}
