import { HttpException } from '@nestjs/common';

export type BaseExceptionError = {
  message: string;
  parent?: string;
};

export class BaseException extends HttpException {
  private readonly errors;

  constructor(name: string, status: number, errors: BaseExceptionError | BaseExceptionError[]) {
    super(name, status);

    if (Array.isArray(errors)) {
      this.errors = errors;
    } else {
      this.errors = [errors];
    }
  }

  public getName = (): string => this.message;

  public getPayload = (): any => this.errors;
}
