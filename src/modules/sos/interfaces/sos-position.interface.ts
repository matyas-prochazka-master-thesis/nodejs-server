import { ISos } from './sos.interface';

export interface ISosPosition {
  id: number;
  sos: ISos;

  latitude: number;
  longitude: number;

  createdAt: Date;
}
