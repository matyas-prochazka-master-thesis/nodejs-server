import { registerEnumType } from '@nestjs/graphql';

export enum ForgottenPasswordSourcesEnum {
  IOS_APP = 'ios_app',
  ANDROID_APP = 'android_app',
}

registerEnumType(ForgottenPasswordSourcesEnum, { name: 'ForgottenPasswordSources' });
