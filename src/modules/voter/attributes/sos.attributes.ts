export enum SosAttributes {
  UPDATE = 'sos_update',
  VIEW = 'sos_view',
}
