import { registerEnumType } from '@nestjs/graphql';

export enum AccountSourceEnum {
  IOS = 'ios',
  ANDROID = 'android',
}

registerEnumType(AccountSourceEnum, { name: 'AccountSource' });
