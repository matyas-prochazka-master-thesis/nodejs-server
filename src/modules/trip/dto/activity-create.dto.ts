import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ActivityCreateDto {
  @Field({ nullable: true })
  readonly latitude: number | null;

  @Field({ nullable: true })
  readonly longitude: number | null;

  @Field({ nullable: true })
  readonly batteryLevel: number | null;
}
