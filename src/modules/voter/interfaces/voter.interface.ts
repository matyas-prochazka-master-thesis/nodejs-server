import { Injectable } from '@nestjs/common';

import { IAccount } from '../../account/interfaces';
import { VoterService } from '../voter.service';

@Injectable()
export abstract class IVoter {
  constructor(protected readonly voterService: VoterService) {
    this.voterService.register(this);
  }

  async isGranted(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    if (!(await this.supports(attribute))) {
      return false;
    }

    return this.voteOnAttribute(attribute, subject, account);
  }

  abstract supports(attribute: string): Promise<boolean>;

  abstract voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean>;
}
