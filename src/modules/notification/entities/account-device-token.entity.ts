import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { IAccountDeviceToken } from '../interfaces';

@ObjectType()
@Entity()
export class AccountDeviceToken implements IAccountDeviceToken {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Account)
  @ManyToOne(() => Account, { nullable: false })
  account: IAccount;

  @Field()
  @Column()
  deviceToken: string;

  @Column('timestamptz')
  addedAt: Date;

  @Column('timestamptz')
  updatedAt: Date;

  constructor(props: { account: IAccount; deviceToken: string }) {
    this.account = props?.account;
    this.deviceToken = props?.deviceToken;

    this.addedAt = new Date();
    this.updatedAt = new Date();
  }
}
