import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

import { BadRequestException } from '../exceptions';
import { BaseExceptionError } from '../exceptions/base.exception';

// From NestJS Docs
@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value, metadata: ArgumentMetadata) {
    if (!value) {
      throw new BadRequestException({ message: 'No data submitted' });
    }

    const { metatype } = metadata;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length > 0) {
      throw new BadRequestException(this.formatError(errors));
    }

    return value;
  }

  private formatError(errors, parent?: string): BaseExceptionError[] {
    let formattedErrors = [];

    errors.forEach((el) => {
      const prop = el.property;

      if (el.children) {
        formattedErrors = [...formattedErrors, ...this.formatError(el.children, prop)];
      }

      if (el.constraints) {
        Object.entries(el.constraints).forEach((constraint) => {
          formattedErrors.push({
            message: `${constraint[1]}`,
            parent: parent ? `${parent}.${prop}` : prop,
          });
        });
      }
    });

    return formattedErrors;
  }

  private buildError(errors, prefix = '') {
    let result = {};
    errors.forEach((el) => {
      const prop = el.property;

      if (el.children) {
        result = {
          ...result,
          ...this.buildError(el.children, prop),
        };
      }

      if (el.constraints) {
        Object.entries(el.constraints).forEach((constraint) => {
          if (prefix) {
            if (!result[prefix]) {
              result[prefix] = {};
            }

            result[prefix][prop] = `${constraint[1]}`;
          } else {
            result[prop] = `${constraint[1]}`;
          }
        });
      }
    });

    return result;
  }

  private toValidate(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];

    return !types.find((type) => metatype === type);
  }
}
