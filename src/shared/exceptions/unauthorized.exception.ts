import { HttpStatus } from '@nestjs/common';

import { BaseException, BaseExceptionError } from './base.exception';

export class UnauthorizedException extends BaseException {
  constructor(errors: BaseExceptionError | BaseExceptionError[]) {
    super('Unauthorized', HttpStatus.UNAUTHORIZED, errors);
  }
}
