export class FakeSending {
  send(one: any, two: any) {}

  sendMulticast(one: any, two: any) {}

  sendToCondition(one: string, two: any) {}

  subscribeToTopic(one: string, two: any) {}

  unsubscribeFromTopic(one: string, two: any) {}
}

export class FirebaseFake {
  messaging(): FakeSending {
    return new FakeSending();
  }
}
