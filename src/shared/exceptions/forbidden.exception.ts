import { HttpStatus } from '@nestjs/common';

import { BaseException, BaseExceptionError } from './base.exception';

export class ForbiddenException extends BaseException {
  constructor(errors: BaseExceptionError | BaseExceptionError[]) {
    super('Forbidden', HttpStatus.FORBIDDEN, errors);
  }
}
