import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { configService } from '../../../config/config.service';
import { UnauthorizedException } from '../../../shared/exceptions';
import { IAccount } from '../../account/interfaces';
import { AccountService } from '../../account/services';
import { TokenDto } from '../dto';
import { JwtTypeEnum } from '../enums';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly accountService: AccountService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.getJwt().secret,
    });
  }

  async validate(payload: TokenDto): Promise<IAccount> {
    if (JwtTypeEnum.ACCESS !== payload.type) {
      throw new UnauthorizedException({ message: 'wrong token type' });
    }

    return this.accountService.find({ accountId: payload.sub });
  }
}
