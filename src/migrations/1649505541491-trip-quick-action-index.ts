import {MigrationInterface, QueryRunner} from "typeorm";

export class tripQuickActionIndex1649505541491 implements MigrationInterface {
    name = 'tripQuickActionIndex1649505541491'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip" ADD "currentQuickActionIndex" integer NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "trip" DROP COLUMN "currentQuickActionIndex"`);
    }

}
