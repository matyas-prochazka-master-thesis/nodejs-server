import { IAccount } from '../../account/interfaces';

export interface IGuardian {
  id: number;
  account: IAccount;
  guardian: IAccount;

  acceptedAt: Date | null;
  createdAt: Date;
}
