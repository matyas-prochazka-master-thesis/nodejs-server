import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { ICheckinRequest, ICheckinRequestGuardian } from '../interfaces';
import { CheckinRequest } from './checkin-request.entity';

@ObjectType()
@Entity()
export class CheckinRequestGuardian implements ICheckinRequestGuardian {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => CheckinRequest)
  @ManyToOne(() => CheckinRequest)
  request: ICheckinRequest;

  @Field(() => Account)
  @ManyToOne(() => Account)
  account: IAccount;

  constructor(props: { request: ICheckinRequest; account: IAccount }) {
    this.request = props?.request;
    this.account = props?.account;
  }
}
