import { IAccount } from '../../account/interfaces';
import { TripQuickActionDto } from '../dto';
import { TripStateEnum } from '../enums';
import { ITripActivity } from './trip-activity.interface';
import { ITripDestination } from './trip-destination.interface';

export interface ITrip {
  id: number;

  account: IAccount;

  state: TripStateEnum;

  shouldArriveAt: Date | null;
  destination: ITripDestination | null;

  quickActions: TripQuickActionDto[];
  currentQuickActionIndex: number;

  createdAt: Date;

  activities: ITripActivity[];
}
