import { BadRequestException, ForbiddenException, NotFoundException } from '../../../shared/exceptions';

const createCheckinRequestNotFound = () => {
  return new NotFoundException({ message: 'Request not found' });
};

const createCheckinNotFound = () => {
  return new NotFoundException({ message: 'Checking not found' });
};

const createCannotRequest = () => {
  return new ForbiddenException({ message: 'Cannot request' });
};

const createBadRangeInPeriodicalRequest = () => {
  return new BadRequestException({ message: 'Bad time range' });
};

const createPeriodInPeriodicalRequest = () => {
  return new BadRequestException({ message: 'Bad period' });
};

const createCheckinRequestCannotView = () => {
  return new ForbiddenException({ message: 'Cannot check' });
};

const createCheckinRequestCannotUpdate = () => {
  return new ForbiddenException({ message: 'Cannot check' });
};

const createCannotCheck = () => {
  return new ForbiddenException({ message: 'Cannot check' });
};

export default {
  createCheckinRequestNotFound,
  createCheckinNotFound,
  createCannotRequest,
  createBadRangeInPeriodicalRequest,
  createPeriodInPeriodicalRequest,
  createCheckinRequestCannotView,
  createCheckinRequestCannotUpdate,
  createCannotCheck,
};
