import { MigrationInterface, QueryRunner } from 'typeorm';

export class accountNameTelephone1648828573657 implements MigrationInterface {
  name = 'accountNameTelephone1648828573657';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "account" ADD "name" character varying`);
    await queryRunner.query(`ALTER TABLE "account" ADD "telephone" character varying`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "telephone"`);
    await queryRunner.query(`ALTER TABLE "account" DROP COLUMN "name"`);
  }
}
