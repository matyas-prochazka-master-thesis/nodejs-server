import { Field, InputType, Int } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class ActivityQuickActionPayload {
  @Field()
  @IsNotEmpty()
  readonly name: string;

  @Field()
  @IsNotEmpty()
  readonly icon: string;

  @Field(() => Int, { nullable: true })
  readonly currentQuickActionIndex: number | null;
}
