import { HttpStatus } from '@nestjs/common';

import { BaseException, BaseExceptionError } from './base.exception';

export class ConflictException extends BaseException {
  constructor(errors: BaseExceptionError | BaseExceptionError[]) {
    super('Conflict', HttpStatus.CONFLICT, errors);
  }
}
