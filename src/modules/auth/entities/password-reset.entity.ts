import { addHours } from 'date-fns';
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';

@Entity()
export class PasswordReset {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Account)
  @JoinColumn()
  account: IAccount;

  @Column()
  code: string;

  @Column()
  expiresAt: Date;

  constructor(props: { account: IAccount; code: string }) {
    this.account = props?.account;
    this.code = props?.code;

    this.expiresAt = addHours(new Date(), 1);
  }
}
