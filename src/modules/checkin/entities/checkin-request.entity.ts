import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { CheckinRequestStateEnum, CheckinRequestTypeEnum } from '../enums';
import { ICheckin, ICheckinRequest, ICheckinRequestGuardian } from '../interfaces';
import { CheckinRequestGuardian } from './checkin-request-guardian.entity';
import { Checkin } from './checkin.entity';

@ObjectType()
@Entity()
export class CheckinRequest implements ICheckinRequest {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Account)
  @ManyToOne(() => Account)
  requester: IAccount;

  @Field(() => Account)
  @ManyToOne(() => Account)
  targetAccount: IAccount;

  @Field(() => CheckinRequestTypeEnum)
  @Column()
  type: CheckinRequestTypeEnum;

  @Field(() => CheckinRequestStateEnum)
  @Column()
  state: CheckinRequestStateEnum;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  startsAt: Date | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  endsAt: Date | null;

  @Field({ nullable: true })
  @Column({ nullable: true })
  periodInMinutes: number | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  nextCheckinAt: Date | null;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @Field(() => [CheckinRequestGuardian])
  @OneToMany(() => CheckinRequestGuardian, (guardian) => guardian.request)
  guardians: ICheckinRequestGuardian[];

  @Field(() => [Checkin])
  @OneToMany(() => Checkin, (checkin) => checkin.request)
  checkins: ICheckin[];

  constructor(props: {
    requester: IAccount;
    targetAccount: IAccount;
    type: CheckinRequestTypeEnum;
    startsAt: Date | null;
    endsAt: Date | null;
    periodInMinutes: number | null;
    nextCheckinAt: Date | null;
  }) {
    this.requester = props?.requester;
    this.targetAccount = props?.targetAccount;
    this.type = props?.type;
    this.startsAt = props?.startsAt;
    this.endsAt = props?.endsAt;
    this.periodInMinutes = props?.periodInMinutes;
    this.nextCheckinAt = props?.nextCheckinAt;

    this.state = CheckinRequestStateEnum.REQUESTED;
  }
}
