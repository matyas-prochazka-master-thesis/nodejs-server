import { registerEnumType } from '@nestjs/graphql';

export enum SosStateEnum {
  HOLDING = 'holding',
  RELEASED = 'released',
  PIN_ENTERED = 'pin_entered',
  EMERGENCY = 'emergency',
}

registerEnumType(SosStateEnum, { name: 'SosState' });
