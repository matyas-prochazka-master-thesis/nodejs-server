import { ISos } from '../interfaces';

const createSosEmergency = ({ sos }: { sos: ISos }) => {
  return {
    title: `${sos.account.username ?? sos.account.email} is in DANGER!`,
    body: `Your friend pressed the emergency button but did not entered their PIN`,
    payload: {
      type: 'danger',
      'localized-title-en': `${sos.account.username ?? sos.account.email} is in DANGER!`,
      'localized-body-en': `Your friend pressed the emergency button but did not entered their PIN`,
    },
  };
};

export default {
  createSosEmergency,
};
