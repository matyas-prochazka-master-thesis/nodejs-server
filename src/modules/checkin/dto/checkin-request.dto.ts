import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CheckinRequestDto {
  @Field(() => ID)
  @IsNotEmpty()
  readonly targetAccountId: number;
}
