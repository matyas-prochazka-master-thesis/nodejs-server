import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { SosStateEnum } from '../enums';
import { ISos } from '../interfaces';

@ObjectType()
@Entity()
export class Sos implements ISos {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  // idFromClient is used to reference the entity so that mobile app does not have to wait until the first request is satisfied
  @Field(() => ID)
  @Column({ unique: true })
  idFromClient: string;

  @Field(() => Account)
  @ManyToOne(() => Account)
  account: IAccount;

  @Field(() => SosStateEnum)
  @Column()
  state: SosStateEnum;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  initialLatitude: number | null;

  @Field({ nullable: true })
  @Column('decimal', { nullable: true })
  initialLongitude: number | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  buttonPressedAt: Date | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  buttonReleasedAt: Date | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  pinEnteredAt: Date | null;

  @Field({ nullable: true })
  @Column('timestamptz', { nullable: true })
  guardiansContactedAt: Date | null;

  @Field()
  @Column('timestamptz', { default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  constructor(props: {
    idFromClient;
    account: IAccount;
    initialLatitude: number | null;
    initialLongitude: number | null;
  }) {
    this.idFromClient = props?.idFromClient;
    this.account = props?.account;
    this.initialLatitude = props?.initialLatitude;
    this.initialLongitude = props?.initialLongitude;

    this.state = SosStateEnum.HOLDING;
  }
}
