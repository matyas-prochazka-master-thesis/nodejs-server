import { IFile } from '../../storage/interfaces';
import { AccountRolesEnum, AccountSourceEnum } from '../enums';

export interface IAccount {
  id: number;
  username: string | null;
  email: string;
  name: string | null;
  telephone: string | null;

  password: string | null;
  role: AccountRolesEnum;

  profilePicture: IFile | null;

  source: AccountSourceEnum | null;

  appleId: string | null;
  facebookId: string | null;
  googleId: string | null;

  registeredAt: Date;

  setPassword(password: string): Promise<IAccount>;
}
