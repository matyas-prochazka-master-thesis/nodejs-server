import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class ActivityMessagePayload {
  @Field()
  @IsNotEmpty()
  readonly message: string;
}
