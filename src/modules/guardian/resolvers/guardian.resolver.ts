import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { IAccount } from '../../account/interfaces';
import { GqlAuthJwtGuard } from '../../auth/guards';
import { GuardianAddDto, GuardianDeclineDto, GuardianRemoveDto, GuardianAcceptDto } from '../dto';
import { Guardian } from '../entities';
import { GuardianService } from '../services';

@Resolver(() => Guardian)
@UseFilters(GqlFilter)
@UseGuards(GqlAuthJwtGuard)
export class GuardianResolver {
  constructor(private readonly guardianService: GuardianService) {}

  @Query(() => [Guardian])
  async getMyGuardians(@GqlAccount() account: IAccount) {
    return this.guardianService.getGuardiansByAccount({ account });
  }

  @Query(() => [Guardian])
  async getGuardiansRequests(@GqlAccount() account: IAccount) {
    return this.guardianService.getGuardiansRequests({ account });
  }

  @Mutation(() => Boolean)
  async acceptGuardian(@GqlAccount() account: IAccount, @Args('guardianAccept') guardianAcceptDto: GuardianAcceptDto) {
    await this.guardianService.acceptGuardianShip({ account, guardianAcceptDto });

    return true;
  }

  @Mutation(() => Boolean)
  async declineGuardian(
    @GqlAccount() account: IAccount,
    @Args('guardianDecline') guardianDeclineDto: GuardianDeclineDto,
  ) {
    await this.guardianService.declineGuardianShip({ account, guardianDeclineDto });

    return true;
  }

  @Mutation(() => Boolean)
  async addGuardian(@GqlAccount() account: IAccount, @Args('guardianAdd') guardianAddDto: GuardianAddDto) {
    await this.guardianService.addGuardian({ account, guardianAddDto });

    return true;
  }

  @Mutation(() => Boolean)
  async removeGuardian(@GqlAccount() account: IAccount, @Args('guardianRemove') guardianRemoveDto: GuardianRemoveDto) {
    await this.guardianService.removeGuardian({ account, guardianRemoveDto });

    return true;
  }
}
