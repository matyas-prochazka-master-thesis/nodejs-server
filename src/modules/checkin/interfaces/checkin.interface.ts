import { CheckinStateEnum } from '../enums';
import { ICheckinRequest } from './checkin-request.interface';

export interface ICheckin {
  id: number;

  request: ICheckinRequest;

  state: CheckinStateEnum;

  latitude: number | null;
  longitude: number | null;

  checkedAt: Date;
  createdAt: Date;
}
