import { ICheckin, ICheckinRequest } from '../interfaces';

const createCheckin = ({ request, checkin }: { request: ICheckinRequest; checkin: ICheckin }) => {
  return {
    title: `${request.requester.username ?? request.requester.email} asks if you are safe?`,
    body: `Your friend sent a checkin request`,
    payload: {
      type: 'info',
      open: 'screen-checkins',
      checkinId: `${checkin.id}`,
      'localized-title-en': `${request.requester.username ?? request.requester.email} asks if you are safe?`,
      'localized-body-en': `Your friend send a checkin request`,
    },
  };
};

const createStartedPeriodicalCheckin = ({ request }: { request: ICheckinRequest }) => {
  return {
    title: `${request.requester.username ?? request.requester.email} just started periodical checkin for you!`,
    body: `Your friend started periodical checkin for you`,
    payload: {
      type: 'warning',
      open: 'screen-checkins',
      'localized-title-en': `${
        request.requester.username ?? request.requester.email
      } just started periodical checkin for you!`,
      'localized-body-en': `Your friend started periodical checkin for you`,
    },
  };
};

const createCheckinCheck = ({ checkin }: { checkin: ICheckin }) => {
  return {
    title: `${checkin.request.targetAccount.username ?? checkin.request.targetAccount.email} just checked in!`,
    body: `Your friend just checked`,
    payload: {
      type: 'success',
      open: 'screen-checkins',
      'localized-title-en': `${
        checkin.request.targetAccount.username ?? checkin.request.targetAccount.email
      } just checked in!`,
      'localized-body-en': `Your friend just checked`,
    },
  };
};

export default {
  createCheckin,
  createStartedPeriodicalCheckin,
  createCheckinCheck,
};
