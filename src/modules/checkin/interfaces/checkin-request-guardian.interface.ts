import { IAccount } from '../../account/interfaces';
import { ICheckinRequest } from './checkin-request.interface';

export interface ICheckinRequestGuardian {
  id: number;

  request: ICheckinRequest;
  account: IAccount;
}
