import { BadRequestException, ForbiddenException, NotFoundException } from '../../../shared/exceptions';

const createGuardianNotFound = () => {
  return new NotFoundException({ message: 'Guardian not found' });
};

const createCannotRemoveGuardian = () => {
  return new ForbiddenException({ message: 'Cannot remove guardian' });
};

const createCannotAcceptGuardian = () => {
  return new ForbiddenException({ message: 'Cannot accept guardian' });
};

const createCannotDeclineGuardian = () => {
  return new ForbiddenException({ message: 'Cannot decline guardian' });
};

const createAlreadyYourGuardian = () => {
  return new BadRequestException({ message: 'Already your guardian' });
};

export default {
  createGuardianNotFound,
  createCannotRemoveGuardian,
  createCannotAcceptGuardian,
  createCannotDeclineGuardian,
  createAlreadyYourGuardian,
};
