import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { IAccount } from '../../account/interfaces';
import { GqlAuthJwtGuard } from '../../auth/guards';
import { SosButtonPressedDto, SosButtonReleasedDto, SosPinEnteredDto, SosPositionUpdateDto } from '../dto';
import { Sos, SosPosition } from '../entities';
import { SosService } from '../services';

@Resolver(() => Sos)
@UseFilters(GqlFilter)
@UseGuards(GqlAuthJwtGuard)
export class SosResolver {
  constructor(private readonly sosService: SosService) {}

  @Mutation(() => Boolean)
  async sosButtonPressed(
    @GqlAccount() account: IAccount,
    @Args('buttonPressed') sosButtonPressedDto: SosButtonPressedDto,
  ) {
    await this.sosService.sosButtonPressed({ account, sosButtonPressedDto });

    return true;
  }

  @Mutation(() => Boolean)
  async sosButtonReleased(
    @GqlAccount() account: IAccount,
    @Args('buttonReleased') sosButtonReleasedDto: SosButtonReleasedDto,
  ) {
    await this.sosService.sosButtonReleased({ account, sosButtonReleasedDto });

    return true;
  }

  @Mutation(() => Boolean)
  async sosPinEntered(@GqlAccount() account: IAccount, @Args('pinEntered') sosPinEnteredDto: SosPinEnteredDto) {
    await this.sosService.sosPinEntered({ account, sosPinEnteredDto });

    return true;
  }

  @Mutation(() => Boolean)
  async sosPositionUpdate(
    @GqlAccount() account: IAccount,
    @Args('positionUpdate') sosPositionUpdateDto: SosPositionUpdateDto,
  ) {
    await this.sosService.sosPositionUpdate({ account, sosPositionUpdateDto });

    return true;
  }

  @Query(() => [Sos])
  async getMySosHistory(@GqlAccount() account: IAccount) {
    return this.sosService.getSosHistory({ account });
  }

  @Query(() => Sos)
  async getLatestSosByAccount(
    @GqlAccount() account: IAccount,
    @Args('targetAccountId', { type: () => ID! }) targetAccountId: number,
  ) {
    return this.sosService.getLatestByAccount({ account, targetAccountId });
  }

  @Query(() => [SosPosition])
  async getSosPositions(@GqlAccount() account: IAccount, @Args('sosId', { type: () => ID! }) sosId: number) {
    return this.sosService.getSosPositions({ account, sosId });
  }

  @Query(() => [Sos])
  async getSosesOfAccountsIAmGuardianOf(@GqlAccount() account: IAccount) {
    return this.sosService.getSosesOfAccountsIAmGuardianOf({ account });
  }
}
