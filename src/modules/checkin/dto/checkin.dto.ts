import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CheckinDto {
  @Field(() => ID)
  @IsNotEmpty()
  readonly checkinId: number;

  @Field({ nullable: true })
  readonly latitude: number | null;

  @Field({ nullable: true })
  readonly longitude: number | null;
}
