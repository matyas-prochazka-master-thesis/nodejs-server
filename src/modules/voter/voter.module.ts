import { Global, Module } from '@nestjs/common';

import { VoterService } from './voter.service';

@Global()
@Module({
  imports: [],
  providers: [VoterService],
  exports: [VoterService],
})
export class VoterModule {}
