import { Inject } from '@nestjs/common';

import { IAccount } from '../../account/interfaces';
import { GuardianService } from '../../guardian/services';
import { SosAttributes } from '../../voter/attributes';
import { IVoter } from '../../voter/interfaces';
import { ISos } from '../interfaces';

export class SosVoter extends IVoter {
  @Inject(GuardianService)
  private readonly guardianService: GuardianService;

  async supports(attribute: string): Promise<boolean> {
    return (<any>Object).values(SosAttributes).includes(attribute);
  }

  async voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    switch (attribute) {
      case SosAttributes.UPDATE:
        return this.canUpdate(account, subject);
      case SosAttributes.VIEW:
        return this.canView(account, subject);
    }

    return false;
  }

  async canUpdate(account: IAccount, sos: ISos): Promise<boolean> {
    return account.id === sos.account.id;
  }

  async canView(account: IAccount, sos: ISos): Promise<boolean> {
    if (account.id === sos.account.id) {
      return true;
    }

    const guardians = await this.guardianService.getGuardiansByAccount({ account: sos.account });

    return guardians.some((guardian) => account.id === guardian.guardian.id);
  }
}
