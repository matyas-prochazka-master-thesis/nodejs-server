import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAccount } from '../../../shared/decorators/gql-account.decorator';
import { ForbiddenException } from '../../../shared/exceptions';
import { GqlFilter } from '../../../shared/filters/gql-exception.filter';
import { Account } from '../../account/entities';
import { IAccount } from '../../account/interfaces';
import { AccountService } from '../../account/services';
import {
  AppleAuthDto,
  AuthReturnDto,
  ForgottenPasswordDto,
  GoogleAuthDto,
  LoginLocalDto,
  PasswordEditDto,
  PasswordResetDto,
  RegisterLocalDto,
  FacebookAuthDto,
} from '../dto';
import { GqlAuthJwtGuard, GqlAuthJwtRefreshGuard } from '../guards';
import { AppleAuthService, AuthService, FacebookAuthService, GoogleAuthService } from '../services';

@Resolver()
@UseFilters(GqlFilter)
export class AuthResolver {
  constructor(
    private readonly accountService: AccountService,
    private readonly appleAuthService: AppleAuthService,
    private readonly authService: AuthService,
    private readonly facebookAuthService: FacebookAuthService,
    private readonly googleAuthService: GoogleAuthService,
  ) {}

  @Mutation(() => AuthReturnDto)
  async login(@Args('login') loginLocalDto: LoginLocalDto) {
    const account = await this.authService.validateAccount(loginLocalDto);

    if (!account) {
      throw new ForbiddenException({ message: 'Invalid credentials' });
    }

    return this.authService.createToken(account);
  }

  @Mutation(() => AuthReturnDto)
  async register(@Args('register') registerLocalDto: RegisterLocalDto) {
    const account = await this.accountService.create({ registerLocalDto });

    return this.authService.createToken(account);
  }

  @Mutation(() => Boolean)
  @UseGuards(GqlAuthJwtGuard)
  async editPassword(@GqlAccount() account: IAccount, @Args('password') passwordEditDto: PasswordEditDto) {
    await this.authService.editPassword(account, passwordEditDto);

    return true;
  }

  @UseGuards(GqlAuthJwtGuard)
  @Query(() => Account)
  async me(@GqlAccount() account: IAccount) {
    return account;
  }

  @Mutation(() => AuthReturnDto)
  @UseGuards(GqlAuthJwtRefreshGuard)
  async refresh(@GqlAccount() account: IAccount) {
    return this.authService.createToken(account);
  }

  @Mutation(() => Boolean)
  async forgottenPassword(@Args('passwordForget') forgottenPasswordDto: ForgottenPasswordDto) {
    await this.authService.forgottenPasswordEmail(forgottenPasswordDto);

    return true;
  }

  @Mutation(() => Boolean)
  async resetPassword(@Args('passwordReset') passwordResetDto: PasswordResetDto) {
    await this.authService.resetPassword(passwordResetDto);

    return true;
  }

  @Mutation(() => AuthReturnDto)
  async appleAuth(@Args('appleAuth') appleAuthDto: AppleAuthDto) {
    return this.appleAuthService.auth(appleAuthDto);
  }

  @Mutation(() => AuthReturnDto)
  async facebookAuth(@Args('accessToken') accessToken: string) {
    return this.facebookAuthService.auth({ facebookAuthDto: { accessToken, source: null } });
  }

  @Mutation(() => AuthReturnDto)
  async facebookAuthV2(@Args('facebookAuth') facebookAuthDto: FacebookAuthDto) {
    return this.facebookAuthService.auth({ facebookAuthDto });
  }

  @Mutation(() => AuthReturnDto)
  async googleAuth(@Args('idToken') idToken: string) {
    return this.googleAuthService.auth({ googleAuthDto: { idToken, source: null } });
  }

  @Mutation(() => AuthReturnDto)
  async googleAuthV2(@Args('googleAuth') googleAuthDto: GoogleAuthDto) {
    return this.googleAuthService.auth({ googleAuthDto });
  }
}
