import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class GuardianAcceptDto {
  @Field(() => ID!)
  @IsNotEmpty()
  readonly guardianId: number;
}
