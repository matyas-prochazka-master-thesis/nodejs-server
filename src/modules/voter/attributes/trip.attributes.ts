export enum TripAttributes {
  UPDATE = 'trip_update',
  VIEW = 'trip_view',
  DESTINATION_VIEW = 'trip_destination_view',
  DESTINATION_FAVOURITE = 'trip_destination_favourite',
}
